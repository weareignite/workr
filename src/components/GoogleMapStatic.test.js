import React from 'react';
import { shallow } from 'enzyme';
import GoogleMapStatic from './GoogleMapStatic';
import config from '../config';

it('renders an empty div if no coordinates provided', () => {
    const wrapper = shallow(<GoogleMapStatic />);
    expect.assertions(2);
    expect( wrapper.find('div').length ).toBe(1);
    expect( wrapper.find('div').children().exists() ).toBe(false);
});

it('renders a google map when provided with an address', () => {
    const wrapper = shallow(<GoogleMapStatic address="London" />);
    expect.assertions(3);
    expect( wrapper.find('div').children().exists() ).toBe(true);
    expect( wrapper.find('img').length ).toBe(1);
    let srcRegex = new RegExp('https:\/\/maps.googleapis.com\/maps\/api\/staticmap\?center=London\&zoom=13\&size=600x600\&style=feature:poi|visibility:off\&style=feature:poi.park|visibility:on\&markers=London\&key=' + config.google.keys.staticMaps);
    expect( wrapper.find('img').props().src ).toMatch(srcRegex);
});

it('google map has google-map-static class', () => {
    const wrapper = shallow(<GoogleMapStatic address="Edinburgh" />);
    expect( wrapper.find('img').hasClass('google-map-static') ).toBe(true);
});
