import React from 'react';
import { shallow } from 'enzyme';
import { Badge } from 'reactstrap';
import TagCloud from './TagCloud';

it('displays a list of Badges when given tags', () => {
    const tags = ['chef', 'pot wash', 'waitor'];
    const wrapper = shallow(<TagCloud tags={tags} />);
    expect( wrapper.find(Badge).length ).toBe(3);
});
