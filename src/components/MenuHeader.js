import React from 'react';

function MenuHeader(props) {
    return (
        <div className="menu-header">
            <img className="rounded-circle" src={props.image} alt=""/>
            {props.children}
        </div>
    );
}

export default MenuHeader;
