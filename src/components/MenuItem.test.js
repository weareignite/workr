import React from 'react';
import { shallow } from 'enzyme';
import FontAwesome from 'react-fontawesome';
import MenuItem from './MenuItem';

it('renders wrapper div with correct class', () => {
    const wrapper = shallow(<MenuItem />);
    expect( wrapper.find('li').hasClass('menu-item') ).toBe(true);
});

it('renders FontAwesome icon', () => {
    const icon = 'star';
    const wrapper = shallow(<MenuItem icon={icon} />);
    expect( wrapper.find(FontAwesome).props() ).toHaveProperty('name', icon);
});

it('displays child text', () => {
    const example = 'Menu Item Example';
    const wrapper = shallow(<MenuItem>{example}</MenuItem>);
    expect( wrapper.children().at(1).text() ).toBe(example);
});

it('triggers onClick event when clicked', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<MenuItem onClick={onClick} />);
    wrapper.simulate('click');
    expect( onClick.mock.calls.length ).toBe(1);
});
