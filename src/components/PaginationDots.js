import React from 'react';
import './pagination-dots.css';

function PaginationDots(props) {
    let dots = [],
        pageCount = props.pageCount ? props.pageCount : 1,
        currentPage = props.currentPage ? props.currentPage : 1;
    for ( let p = 1; p <= pageCount; p++ ) {
        let label = 'page-dot';
        if ( currentPage === p ) {
            label += ' active';
        }
        dots.push(<div key={p} className={label} onClick={ event => props.onSelectPage(p, event) } />);
    }
    return (
        <div className="pagination-dots">
            {dots}
        </div>
    );
}

export default PaginationDots;
