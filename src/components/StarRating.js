import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import './star-rating.css';

class StarRating extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: props.rating ? props.rating : 0,
            hoverRating: 0,
        };
    }

    handleChange(rating, event) {
        if ( this.props.onChange ) {
            const onChangeObservers = Array.isArray(this.props.onChange) ? this.props.onChange : [this.props.onChange];
            onChangeObservers.forEach(onChange => onChange(rating, event));
        }
    }

    handleClick(rating, event) {
        this.setState({ rating: rating });
        this.handleChange(rating, event);
    }

    handleMouseOver(rating) {
        this.setState({ hoverRating: rating });
    }

    handleMouseOut() {
        this.setState({ hoverRating: 0 });
    }

    render() {
        let currentRating = this.state.hoverRating ? this.state.hoverRating : this.state.rating,
            starRatingPoints = [1, 2, 3, 4, 5],
            stars = starRatingPoints.map(starRatingPoint => {
                let className = 'star';
                if ( starRatingPoint <= Math.floor(currentRating) ) {
                    className += ' highlight';
                }
                return (
                    <div
                        key={starRatingPoint}
                        className={className}
                        onClick={event => this.handleClick(starRatingPoint, event)}
                        onMouseOver={() => this.handleMouseOver(starRatingPoint)}
                        onMouseOut={() => this.handleMouseOut()}>
                        <FontAwesome name='star' size='lg' />
                    </div>
                );
            });
        return (
            <div className={'star-rating' + (this.state.hoverRating ? ' hovering' : '')}>
                {stars}
            </div>
        );
    }
}

export default StarRating;
