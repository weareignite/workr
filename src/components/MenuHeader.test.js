import React from 'react';
import { shallow } from 'enzyme';
import MenuHeader from './MenuHeader';

it('renders wrapper div with correct class', () => {
    const wrapper = shallow(<MenuHeader />);
    expect( wrapper.find('div').hasClass('menu-header') ).toBe(true);
});

it('renders profile img', () => {
    const wrapper = shallow(<MenuHeader />);
    const img = wrapper.find('img');
    expect( img.length ).toBe(1);
    expect( img.hasClass('rounded-circle') ).toBe(true);
});

it('maps props image to img', () => {
    const profileImage = "http://via.placeholder.com/50x50";
    const wrapper = shallow(<MenuHeader image={profileImage} />);
    expect( wrapper.find('img').props() ).toHaveProperty('src', profileImage);
});

it('displays child text, user name', () => {
    const example = 'Matthew Davies';
    const wrapper = shallow(<MenuHeader>{example}</MenuHeader>);
    expect( wrapper.children().at(1).text() ).toBe(example);
});
