import React from 'react';
import FontAwesome from 'react-fontawesome';

function MenuItem(props) {
    return (
        <li className="menu-item" onClick={props.onClick}>
            <FontAwesome fixedWidth name={props.icon ? props.icon : 'star'} />
            {props.children}
        </li>
    );
}

export default MenuItem;
