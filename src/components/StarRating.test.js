import React from 'react';
import { shallow } from 'enzyme';
import StarRating from './StarRating';

it('renders the wrapper div with correct class', () => {
    const wrapper = shallow(<StarRating />);
    expect( wrapper.find('div').at(0).hasClass('star-rating') ).toBe(true);
});

it('renders five stars by default', () => {
    const wrapper = shallow(<StarRating />);
    expect( wrapper.find('div.star').length ).toBe(5);
});

it('gives highlight class to stars up to rating mark', () => {
    const wrapper = shallow(<StarRating rating={3.5} />);
    const stars = wrapper.find('div.star');
    expect( stars.at(0).hasClass('highlight') ).toBe(true);
    expect( stars.at(1).hasClass('highlight') ).toBe(true);
    expect( stars.at(2).hasClass('highlight') ).toBe(true);
    expect( stars.at(3).hasClass('highlight') ).toBe(false);
    expect( stars.at(4).hasClass('highlight') ).toBe(false);
});

it('triggers handleClick when clicking a star', () => {
    const wrapper = shallow(<StarRating />);
    const instance = wrapper.instance();
    instance.handleClick = jest.fn();
    wrapper.find('div.star').at(0).simulate('click');
    expect( instance.handleClick.mock.calls.length ).toBe(1);
});

it('handleClick sets state', () => {
    const wrapper = shallow(<StarRating />);
    wrapper.instance().handleClick(2);
    expect( wrapper.state().rating ).toBe(2);
});

it('triggers handleMouseOver when hovering over a star', () => {
    const wrapper = shallow(<StarRating />);
    const instance = wrapper.instance();
    instance.handleMouseOver = jest.fn();
    wrapper.find('div.star').at(0).simulate('mouseOver');
    expect( instance.handleMouseOver.mock.calls.length ).toBe(1);
});

it('handleMouseOver sets state', () => {
    const wrapper = shallow(<StarRating />);
    wrapper.instance().handleMouseOver(2);
    expect( wrapper.state().hoverRating ).toBe(2);
});

it('triggers handleMouseOut when moving off a star', () => {
    const wrapper = shallow(<StarRating />);
    const instance = wrapper.instance();
    instance.handleMouseOut = jest.fn();
    wrapper.find('div.star').at(0).simulate('mouseOut');
    expect( instance.handleMouseOut.mock.calls.length ).toBe(1);
});

it('handleMouseOut sets state', () => {
    const wrapper = shallow(<StarRating />);
    wrapper.instance().handleMouseOver(2);
    expect( wrapper.state().hoverRating ).toBe(2);
    wrapper.instance().handleMouseOut();
    expect( wrapper.state().hoverRating ).toBe(0);
});

it('adds hovering class when state is set', () => {
    const wrapper = shallow(<StarRating />);
    wrapper.find('div.star').at(0).simulate('mouseOver');
    expect( wrapper.find('div').at(0).hasClass('hovering') ).toBe(true);
});

it('calls onChange callback when change is triggered', () => {
    const onChangeMock = jest.fn();
    const wrapper = shallow(<StarRating onChange={onChangeMock} />);
    wrapper.find('div.star').at(2).simulate('click');
    expect(onChangeMock.mock.calls.length).toBe(1);
});

it('can call multiple onChange callbacks when change is triggered', () => {
    const onChangeMock = [jest.fn(), jest.fn()];
    const wrapper = shallow(<StarRating onChange={onChangeMock} />);
    wrapper.find('div.star').at(2).simulate('click');
    expect(onChangeMock[0].mock.calls.length).toBe(1);
    expect(onChangeMock[1].mock.calls.length).toBe(1);
});
