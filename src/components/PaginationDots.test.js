import React from 'react';
import { shallow } from 'enzyme';
import PaginationDots from './PaginationDots';

it('renders the wrapper div with correct class', () => {
    const wrapper = shallow(<PaginationDots />);
    expect( wrapper.find('div').at(0).hasClass('pagination-dots') ).toBe(true);
});

it('renders just one dot by default', () => {
    const wrapper = shallow(<PaginationDots />);
    expect( wrapper.find('div.page-dot').length ).toBe(1);
    expect( wrapper.find('div.page-dot').at(0).hasClass('active') ).toBe(true);
});

it('renders number of dots based on page count', () => {
    const wrapper = shallow(<PaginationDots pageCount={5} />);
    expect( wrapper.find('div.page-dot').length ).toBe(5);
});

it('gives active class to dot that represents current page', () => {
    const wrapper = shallow(<PaginationDots pageCount={5} currentPage={3} />);
    expect( wrapper.find('div.page-dot').length ).toBe(5);
    expect( wrapper.find('div.page-dot').at(2).hasClass('active') ).toBe(true);
});

it('triggers the onSelectPage event when clicking a dot', () => {
    const onSelectPage = jest.fn();
    const wrapper = shallow(<PaginationDots onSelectPage={onSelectPage} />);
    wrapper.find('div.page-dot').simulate('click');
    expect( onSelectPage.mock.calls.length ).toBe(1);
});

it('passes page number to onSelectPage event', () => {
    const onSelectPage = (pageNumber) => {
        expect( pageNumber ).toBe(2);
    };
    const wrapper = shallow(<PaginationDots pageCount={3} onSelectPage={onSelectPage} />);
    wrapper.find('div.page-dot').at(1).simulate('click');
    expect.hasAssertions();
});
