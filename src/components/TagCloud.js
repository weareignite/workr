import React from 'react';
import { Badge } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import './tag-cloud.css';

function TagCloud(props) {
    let tagCloudList;
    if ( props.tags ) {
        tagCloudList = props.tags.map(tag => <Badge key={tag} onClick={(event) => props.onClick(event, tag)}>{tag} <FontAwesome name='times-circle' /></Badge>);
    }
    return <div className="tag-cloud">{tagCloudList}</div>;
}

export default TagCloud;
