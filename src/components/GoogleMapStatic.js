import React from 'react';
import config from '../config';

function GoogleMapStatic(props) {
    let map, apiUrlParts, apiUrl;
    if ( props.address ) {
        apiUrlParts = [
            'https://maps.googleapis.com/maps/api/staticmap?',
            'center=[address]',
            'zoom=13',
            'size=600x600',
            'style=feature:poi|visibility:off',
            'style=feature:poi.park|visibility:on',
            'markers=[address]',
            'key=[key]'
        ];
        apiUrl = apiUrlParts.join('&')
                            .replace(/\[address\]/g, props.address)
                            .replace(/\[key\]/, config.google.keys.staticMaps);
        map = <img className="google-map-static" src={apiUrl} alt=""/>
    }
    return (
        <div>
            {map}
        </div>
    );
}

export default GoogleMapStatic;
