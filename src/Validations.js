import { Component } from 'react';
import moment from 'moment';

function validate(key, validationExpression, callback) {
    let valid = validationExpression ? true : false;
    if ( typeof callback === 'function' ) {
        callback(key, valid);
    }
    return valid;
}

export function validateRequired(key, value, callback) {
    return validate(key, value.length > 0, callback);
}

export function validateRequiredArray(key, value, callback) {
    return validate(key, Array.isArray(value) && value.length > 0, callback);
}

export function validateEmail(key, value, callback) {
    return validate(key, value.match(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i), callback);
}

export function validateNumber(key, value, callback) {
    return validate(key, typeof value === 'number', callback);
}

export function validateDateRange(key, value, callback) {
    if ( typeof value.start === 'undefined' || typeof value.finish === 'undefined' ) {
        return validate(key, false, callback);
    }
    return validate(key, moment.isMoment(value.start) && moment.isMoment(value.finish), callback);
}

export function validatePassword(key, value, callback) {
    return validate(key, value.match(/\d+/) && value.length > 5, callback);
}

export function validateMatch(key, value, match, callback) {
    return validate(key, value.length > 0 && value === this.state.values[match], callback);
}

function setValidationState(key, valid) {
    if ( typeof this.state === 'object' && Array.isArray(this.state.validations) ) {
        let stateValidations = this.state.validations;
        stateValidations[key] = valid;
        this.setState({ validations: stateValidations });
    }
}

function getValidationErrorMessage(validations, errorMessages, key) {
    if ( validations[key] === false ) {
        return typeof errorMessages[key] !== 'undefined' ? errorMessages[key] : '';
    }
    return null;
}

class ValidatedComponent extends Component {
    constructor(props) {
        super(props);
        this.setValidationState = setValidationState.bind(this);
        this.getValidationErrorMessage = getValidationErrorMessage.bind(this);
    }

    componentWillMount() {
        this.setState({
            validations: []
        });
    }

    isValid(validationMap) {
        let isValid = true;
        const stateValues = this.state.values;
        for ( let key in validationMap ) {
            if ( this.handleValidate(validationMap, key, stateValues[key]) === false ) {
                isValid = false;
            }
        }
        return isValid;
    }

    handleChange(key, value) {
        let state = this.state;
        if ( state.values === 'undefined' ) {
            state.values = {};
        }
        state.values[key] = value;
        this.setState(state);
    }

    handleValidate(validationMap, key, value) {
        let validationFnName = validationMap[key];
        if ( typeof validationFnName === 'undefined' ) {
            return true;
        } else if ( Array.isArray(validationFnName) ) {
            return validationFnName[0].call(this, key, value, validationFnName[1], this.setValidationState);
        } else {
            return validationFnName.call(this, key, value, this.setValidationState);
        }
    }

    handleSubmit(validationMap) {
        if ( this.props.onSubmit && this.isValid(validationMap) ) {
            this.props.onSubmit( this.state.values );
        }
    }
}

export default ValidatedComponent;
