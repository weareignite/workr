import React from 'react';
import { shallow } from 'enzyme';
import ValidatedComponent, { validateRequired, validateRequiredArray, validateEmail, validateNumber, validateDateRange, validatePassword, validateMatch } from './Validations';
import moment from 'moment';

const exampleErrorMessages = {
    name: 'Please enter your full name'
};

class MockComponent extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                test: 'example'
            }
        };
    }
    render() {
        return <div></div>
    }
}

it('can instantiate validated component', () => {
    const wrapper = shallow(<MockComponent />);
    expect( wrapper.find('div').length ).toBe(1);
});

it('sets base state of validated component', () => {
    const wrapper = shallow(<MockComponent />);
    expect( wrapper.state() ).toHaveProperty('validations', []);
});

it('attaches setValidationState', () => {
    const wrapper = shallow(<MockComponent />);
    expect( typeof wrapper.instance().setValidationState ).toBe('function');
});

it('attaches getValidationErrorMessage', () => {
    const wrapper = shallow(<MockComponent />);
    expect( typeof wrapper.instance().getValidationErrorMessage ).toBe('function');
});

it('allows the validation of a input to be set by key', () => {
    const wrapper = shallow(<MockComponent />);
    wrapper.instance().setValidationState('example', true);
    expect( wrapper.state().validations.example ).toBe(true);
});

it('allows validation error messages to be retrieved by key', () => {
    const wrapper = shallow(<MockComponent />);
    const instance = wrapper.instance();
    instance.setValidationState('name', false);
    expect( instance.getValidationErrorMessage(wrapper.state().validations, exampleErrorMessages, 'name') ).toBe('Please enter your full name');
});

it('returns true from handleValidate if mapped fn is undefined', () => {
    const wrapper = shallow(<MockComponent />);
    const map = {};
    expect( wrapper.instance().handleValidate(map, 'test') ).toBe(true);
});

it('returns true from handleValidate if mapped fn returns true', () => {
    const wrapper = shallow(<MockComponent />);
    const map = { test: value => true };
    expect( wrapper.instance().handleValidate(map, 'test') ).toBe(true);
});

it('returns true from handleValidate if mapped fn is array and returns true', () => {
    const wrapper = shallow(<MockComponent />);
    const map = { test: [value => true] };
    expect( wrapper.instance().handleValidate(map, 'test') ).toBe(true);
});

it('calls isValid and onSubmit when handleSubmit is called', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<MockComponent onSubmit={onSubmit} />);
    const instance = wrapper.instance();
    instance.isValid = jest.fn().mockReturnValue(true);
    instance.handleSubmit();
    expect( onSubmit.mock.calls.length ).toBe(1);
    expect( instance.isValid.mock.calls.length ).toBe(1);
});

it('sets state when handleChange is called', () => {
    const wrapper = shallow(<MockComponent />);
    const instance = wrapper.instance();
    instance.handleChange('test', 'example');
    expect( wrapper.state().values ).toHaveProperty('test', 'example');
});


it('does not let handleChange overwrite state values', () => {
    const wrapper = shallow(<MockComponent />);
    const instance = wrapper.instance();
    instance.setState({ values: { alpha: 'foo' } });
    instance.handleChange('beta', 'bar');
    expect( wrapper.state().values ).toHaveProperty('alpha', 'foo');
    expect( wrapper.state().values ).toHaveProperty('beta', 'bar');
});

it('returns true from isValid when validations are true', () => {
    const wrapper = shallow(<MockComponent />);
    const map = { test: value => true };
    expect( wrapper.instance().isValid(map) ).toBe(true);
});

it('calls setValidationState when validating an input', () => {
    const wrapper = shallow(<MockComponent />);
    const instance = wrapper.instance();
    const map = { example: validateRequired };
    instance.setValidationState = jest.fn();
    instance.handleValidate(map, 'example', 'test');
    expect( instance.setValidationState.mock.calls.length ).toBe(1);
});

it('can validate that strings have more than 0 characters', () => {
    const key = 'example';
    expect( validateRequired.call(null, key, '') ).toBe(false);
    expect( validateRequired.call(null, key, 'Matt D') ).toBe(true);
});

it('can validate that emails are roughly formatted correctly', () => {
    const key = 'email';
    expect( validateEmail.call(null, key, 'some random non email string') ).toBe(false);
    expect( validateEmail.call(null, key, 'matt@teamdavies.co.uk') ).toBe(true);
});

it('can validate arrays are valid', () => {
    const key = 'array';
    expect( validateRequiredArray.call(null, key, 'some random non array string') ).toBe(false);
    expect( validateRequiredArray.call(null, key, ['example']) ).toBe(true);
});

it('can validate numbers are valid', () => {
    const key = 'number';
    expect( validateNumber.call(null, key, 'text') ).toBe(false);
    expect( validateNumber.call(null, key, 1) ).toBe(true);
});

it('can validate date ranges are valid', () => {
    const key = 'dateRange';
    expect( validateDateRange.call(null, key, 'text') ).toBe(false);
    expect( validateDateRange.call(null, key, { start: moment() }) ).toBe(false);
    expect( validateDateRange.call(null, key, { finish: moment() }) ).toBe(false);
    expect( validateDateRange.call(null, key, { start: moment(), finish: moment() }) ).toBe(true);
});

it('can validate passwords', () => {
    const key = 'number';
    expect( validatePassword.call(null, key, 'text') ).toBe(false);
    expect( validatePassword.call(null, key, '1') ).toBe(false);
    expect( validatePassword.call(null, key, 'example1') ).toBe(true);
});

it('can validate things match', () => {
    const key = 'number';
    const context = { state: { values: { example: 'one' } } };
    expect( validateMatch.call(context, key, 'text', 'example') ).toBe(false);
    expect( validateMatch.call(context, key, 'one', 'example') ).toBe(true);
});
