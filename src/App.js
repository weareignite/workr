import React, { Component } from 'react';
import Menu from './blocks/Menu';

class App extends Component {
    render() {
        return (
            <div className="container">
                <Menu profileImage="http://via.placeholder.com/50x50" name="Matthew Davies" />
            </div>
        );
    }
}

export default App;
