import React from 'react';
import MenuHeader from '../components/MenuHeader';
import MenuItem from '../components/MenuItem';
import './menu.css';

function Menu(props) {
    return (
        <div className="menu">
            <MenuHeader image={props.profileImage}>{props.name}</MenuHeader>
            <ul>
                <MenuItem icon="home">Dashboard</MenuItem>
                <MenuItem icon="thumbs-up">Find a Worker</MenuItem>
                <MenuItem icon="star">Find a Gig</MenuItem>
                <MenuItem icon="sign-out">Log Out</MenuItem>
            </ul>
        </div>
    );
}

export default Menu;
