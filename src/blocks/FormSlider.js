import React, { Component } from 'react';
import { FormGroup, Label } from 'reactstrap';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import './form-slider.css';

class FormSlider extends Component {
    constructor(props) {
        super(props);
        let min = this.props.min ? this.props.min : 0,
            max = this.props.max ? this.props.max : 100,
            value = this.props.value ? this.props.value : min;
        this.state = {
            min: min,
            max: max,
            value: value,
            valid: null
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleAfterChange = this.handleAfterChange.bind(this);
    }

    handleChange(value) {
        this.setState({ value: value });
    }

    handleAfterChange(value) {
        if ( this.props.onChange ) {
            const onChangeObservers = Array.isArray(this.props.onChange) ? this.props.onChange : [this.props.onChange];
            onChangeObservers.forEach(onChange => onChange(value));
        }
        this.validate();
    }

    validate() {
        if ( this.props.onValidate ) {
            this.setState({
                valid: this.props.onValidate( this.state.tags )
            });
        }
    }

    render() {
        let valid = typeof this.props.valid === 'undefined' ? this.state.valid : this.props.valid,
            label = this.props.label ? <Label className={valid === false ? 'text-danger' : null}>{this.props.label}</Label> : null,
            valueFormat = this.props.valueFormat ? this.props.valueFormat : '%d',
            formattedValue = valueFormat.replace(/%d/, this.state.value);
        return (
            <div className="form-slider">
                <FormGroup>
                    <span className="pull-right">{formattedValue}</span>
                    {label}
                    <Slider
                        defaultValue={this.state.value}
                        min={this.state.min}
                        max={this.state.max}
                        onChange={this.handleChange}
                        onAfterChange={this.handleAfterChange} />
                </FormGroup>
            </div>
        );
    }
}

export default FormSlider;
