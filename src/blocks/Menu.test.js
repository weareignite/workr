import React from 'react';
import { shallow } from 'enzyme';
import Menu from './Menu';
import MenuHeader from '../components/MenuHeader';
import MenuItem from '../components/MenuItem';

it('renders wrapper div with correct class', () => {
    const wrapper = shallow(<Menu />);
    expect( wrapper.find('div').hasClass('menu') ).toBe(true);
});

it('renders MenuHeader', () => {
    const mockData = {
        profile: "http://via.placeholder.com/50x50",
        name: "Matthew Davies"
    };
    const wrapper = shallow(<Menu profileImage={mockData.profile} name={mockData.name} />);
    expect( wrapper.find(MenuHeader).props() ).toHaveProperty('image', mockData.profile);
    expect( wrapper.find(MenuHeader).props() ).toHaveProperty('children', mockData.name);
});

it('renders 4 MenuItems', () => {
    const wrapper = shallow(<Menu />);
    const menuItems = wrapper.find(MenuItem);
    expect( menuItems.length ).toBe(4);
    expect( menuItems.at(0).props() ).toHaveProperty('icon', 'home');
    expect( menuItems.at(0).props() ).toHaveProperty('children', 'Dashboard');
    expect( menuItems.at(1).props() ).toHaveProperty('icon', 'thumbs-up');
    expect( menuItems.at(1).props() ).toHaveProperty('children', 'Find a Worker');
    expect( menuItems.at(2).props() ).toHaveProperty('icon', 'star');
    expect( menuItems.at(2).props() ).toHaveProperty('children', 'Find a Gig');
    expect( menuItems.at(3).props() ).toHaveProperty('icon', 'sign-out');
    expect( menuItems.at(3).props() ).toHaveProperty('children', 'Log Out');
});
