import React, { Component } from 'react';
import { FormGroup, Label, Input, FormText } from 'reactstrap';

class FormInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valid: null // changes to boolean on blur
        };
        this.handleBlur = this.handleBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleBlur(event) {
        if ( this.props.onValidate ) {
            this.setState({
                valid: this.props.onValidate(event.target.value, event)
            });
        }
    }

    handleChange(event) {
        if ( this.props.onChange ) {
            const onChangeObservers = Array.isArray(this.props.onChange) ? this.props.onChange : [this.props.onChange];
            onChangeObservers.forEach(onChange => onChange(event.target.value, event));
        }
    }

    handleKeyPress(event) {
        if ( this.props.onKeyPress ) {
            const onKeyPressObservers = Array.isArray(this.props.onKeyPress) ? this.props.onKeyPress : [this.props.onKeyPress];
            onKeyPressObservers.forEach(onKeyPress => onKeyPress(event.target.value, event));
        }
    }

    render() {
        if ( this.props.label && typeof this.props.id === 'undefined' ) {
            throw new Error('Define FormInput.props.id to ensure Input and Label are linked');
        }
        let inputType = this.props.type ? this.props.type : 'text',
            inputHelpText = this.props.children ? <FormText color="muted">{this.props.children}</FormText> : null,
            inputValid = typeof this.props.valid === 'undefined' ? this.state.valid : this.props.valid,
            inputLabelClass = inputValid === false ? 'text-danger' : null,
            inputLabel = this.props.label ? <Label for={this.props.id} className={inputLabelClass}>{this.props.label}</Label> : null,
            showInput = this.props.showInput === false ? false : true;
        return (
            <FormGroup>
                {inputLabel}
                {showInput ? (<Input
                    type={inputType}
                    id={this.props.id}
                    placeholder={this.props.placeholder}
                    valid={inputValid}
                    value={this.props.value || ''}
                    onBlur={this.handleBlur}
                    onChange={this.handleChange}
                    onKeyPress={this.handleKeyPress} />) : null}
                {inputHelpText}
            </FormGroup>
        );
    }
}

export default FormInput;
