import React, { Component } from 'react';
import { Button } from 'reactstrap';
import FormInput from './FormInput';
import TagCloud from '../components/TagCloud';
import './form-tag-cloud.css';

class FormTagCloud extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valid: null,
            tags: Array.isArray( this.props.tags ) && this.props.tags.length > 0 ? this.props.tags : [],
            tagInputValue: this.props.value ? this.props.value : ''
        };
        this.handleChangeInputValue = this.handleChangeInputValue.bind(this);
        this.handleKeyPressInputValue = this.handleKeyPressInputValue.bind(this);
        this.handleClickAddTag = this.handleClickAddTag.bind(this);
        this.handleClickTag = this.handleClickTag.bind(this);
    }

    handleChangeInputValue(value) {
        this.setState({ tagInputValue: value });
    }

    handleKeyPressInputValue(value, event) {
        if ( event.key === 'Enter' ) {
            this.addTags(event);
        }
    }

    handleClickAddTag(event) {
        this.addTags(event);
    }

    handleClickTag(event, tag) {
        this.removeTag(tag, event);
    }

    addTags(event) {
        let newTags = this.state.tagInputValue.split(','),
            stateTags = this.state.tags;
        if ( Array.isArray(newTags) && newTags.length > 0 ) {
            newTags.forEach(newTag => {
                newTag = newTag.trim();
                if ( newTag.length > 0 && stateTags.indexOf(newTag) === -1 ) {
                    stateTags.push( newTag );
                }
            });
            this.setState({ tags: stateTags, tagInputValue: '' });
            this.notifyTagsUpdate(event);
        }
    }

    removeTag(tag, event) {
        let stateTags = this.state.tags,
            oldTagIndex = stateTags.indexOf(tag);
        if ( oldTagIndex >= 0 ) {
            stateTags.splice( oldTagIndex, 1 );
            this.setState({ tags: stateTags });
            this.notifyTagsUpdate(event);
        }
    }

    notifyTagsUpdate(event) {
        if ( this.props.onChange ) {
            const onChangeObservers = Array.isArray(this.props.onChange) ? this.props.onChange : [this.props.onChange];
            onChangeObservers.forEach(onChange => onChange(this.state.tags, event));
        }
        this.validateTags();
    }

    validateTags() {
        if ( this.props.onValidate ) {
            this.setState({
                valid: this.props.onValidate( this.state.tags )
            });
        }
    }

    render() {
        let tagCloudId = this.props.id ? this.props.id : 'tag-cloud',
            tagCloudLabel = this.props.label ? this.props.label : 'Tags',
            tagCloudHelpText = this.props.children ? this.props.children : 'Separate tags with a comma';
        return (
            <div className="form-tag-cloud">
                <FormInput
                    label={tagCloudLabel}
                    id={tagCloudId}
                    value={this.state.tagInputValue}
                    valid={typeof this.props.valid === 'undefined' ? this.state.valid : this.props.valid}
                    placeholder={this.props.placeholder}
                    onChange={this.handleChangeInputValue}
                    onKeyPress={this.handleKeyPressInputValue}>{tagCloudHelpText}</FormInput>
                <Button className="btn-add-tags" onClick={this.handleClickAddTag}>Add Tags</Button>
                <TagCloud tags={this.state.tags} onClick={this.handleClickTag} />
            </div>
        );
    }

}

export default FormTagCloud;
