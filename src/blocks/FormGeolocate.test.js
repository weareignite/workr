import React from 'react';
import { shallow, mount } from 'enzyme';
import FormGeolocate from './FormGeolocate';
import FormInput from './FormInput';
import GoogleMapStatic from '../components/GoogleMapStatic';

it('renders a div with correct class', () => {
    const wrapper = shallow(<FormGeolocate timeout={0} />);
    expect( wrapper.find('div').at(0).hasClass('form-geolocate') ).toBe(true);
});

it('renders a FormInput', () => {
    const wrapper = shallow(<FormGeolocate timeout={0} />);
    expect( wrapper.find(FormInput).length ).toBe(1);
});

it('renders a GoogleMapStatic', () => {
    const wrapper = shallow(<FormGeolocate timeout={0} />);
    expect( wrapper.find(GoogleMapStatic).length ).toBe(1);
});

it('check form update changes state', () => {
    const wrapper = shallow(<FormGeolocate timeout={0} />);
    const inputValue = 'London';
    wrapper.find(FormInput).simulate('change', inputValue);
    expect(wrapper.state().value).toBe(inputValue);
});

it('check form update changes map value', () => {
    const wrapper = shallow(<FormGeolocate timeout={0} />);
    const inputValue = 'London';
    wrapper.find(FormInput).simulate('change', inputValue);
    expect(wrapper.find(GoogleMapStatic).props().address).toBe(inputValue);
});

it('form update triggers debounce functions when timeout is set', () => {
    const wrapper = shallow(<FormGeolocate />);
    const inputEvent = { target: { value: 'Edinburgh' }, persist: jest.fn() };
    wrapper.instance().debounce = jest.fn();
    wrapper.instance().cancelDebounce = jest.fn();
    wrapper.find(FormInput).simulate('change', inputEvent.target.value, inputEvent);
    expect(wrapper.instance().debounce.mock.calls.length).toBe(1);
    expect(wrapper.instance().cancelDebounce.mock.calls.length).toBe(1);
    expect(inputEvent.persist.mock.calls.length).toBe(1);
});

it('can call debounce', () => {
    const wrapper = shallow(<FormGeolocate />);
    const inputEvent = { target: { value: 'Edinburgh' }, persist: jest.fn() };
    wrapper.find(FormInput).simulate('change', inputEvent.target.value, inputEvent);
    expect( wrapper.instance().debouncedFunction.name ).toBe('debounced');
});

it('can assign a label to child FormInput', () => {
    const label = "Skills";
    const wrapper = mount(<FormGeolocate label={label} id="location" />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('label', label);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('id', 'location');
});

it('can assign a placeholder to child FormInput', () => {
    const placeholder = "this is an example placeholder";
    const wrapper = mount(<FormGeolocate placeholder={placeholder} />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('placeholder', placeholder);
});

it('passes valid to FormInput props', () => {
    const wrapper = shallow(<FormGeolocate valid={false} />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('valid', false);
});

it('passes value to FormInput props', () => {
    const value = "Edinburgh";
    const wrapper = shallow(<FormGeolocate value={value} />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('value', value);
});
