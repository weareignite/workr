import React from 'react';
import { shallow } from 'enzyme';
import RatingCard from './RatingCard';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import StarRating from '../components/StarRating';

const mockData = {
    name: 'Matthew Davies',
    image: 'http://via.placeholder.com/700x500'
};

it('renders a Modal', () => {
    const wrapper = shallow(<RatingCard />);
    const modal = wrapper.find(Modal);
    expect( modal.length ).toBe(1);
    expect( modal.props() ).toHaveProperty('size', 'sm');
    expect( modal.props() ).toHaveProperty('isOpen', false);
    expect( modal.hasClass('rating-card') ).toBe(true);
});

it('renders a ModalHeader', () => {
    const wrapper = shallow(<RatingCard />);
    const modal = wrapper.find(ModalHeader);
    expect( modal.props().toggle.name ).toBe('bound handleClose');
});

it('displays name in ModalHeader', () => {
    const wrapper = shallow(<RatingCard name={mockData.name} />);
    const modal = wrapper.find(ModalHeader);
    expect( modal.children().at(0).text() ).toBe('Rate ');
    expect( modal.children().at(1).text() ).toBe(mockData.name);
});

it('renders a ModalBody', () => {
    const wrapper = shallow(<RatingCard />);
    expect( wrapper.find(ModalBody).length ).toBe(1);
});

it('renders an img', () => {
    const wrapper = shallow(<RatingCard image={mockData.image} />);
    const img = wrapper.find('img');
    expect( img.length ).toBe(1);
    expect( img.props() ).toHaveProperty('src', mockData.image);
    expect( img.props() ).toHaveProperty('className', 'img-fluid');
    expect( img.props() ).toHaveProperty('alt', 'Profile');
});

it('renders the StarRating component', () => {
    const wrapper = shallow(<RatingCard />);
    expect( wrapper.find(StarRating).length ).toBe(1);
});

it('call onChange when StarRaing trigger change', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<RatingCard onChange={onChange} />);
    wrapper.find(StarRating).simulate('change');
    expect( onChange.mock.calls.length ).toBe(1);
});
