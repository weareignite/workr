import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { Badge, Button } from 'reactstrap';
import FormTagCloud from './FormTagCloud';
import FormInput from './FormInput';
import TagCloud from '../components/TagCloud';

it('displays a div with the correct class', () => {
    const wrapper = shallow(<FormTagCloud />);
    expect( wrapper.find('div').at(0).hasClass('form-tag-cloud') ).toBe(true);
});

it('displays a FormInput', () => {
    const wrapper = shallow(<FormTagCloud />);
    expect( wrapper.find(FormInput).length ).toBe(1);
});

it('displays a TagCloud', () => {
    const wrapper = shallow(<FormTagCloud />);
    expect( wrapper.find(TagCloud).length ).toBe(1);
});

it('displays a Button with correct class', () => {
    const wrapper = shallow(<FormTagCloud />);
    expect( wrapper.find(Button).length ).toBe(1);
    expect( wrapper.find(Button).props().className ).toBe('btn-add-tags');
});

it('displays a list of Badges when given tags', () => {
    const tags = ['chef', 'pot wash', 'waitor'];
    const wrapper = mount(<FormTagCloud tags={tags} />);
    expect( wrapper.find(FormInput).length ).toBe(1);
    expect( wrapper.find(Badge).length ).toBe(3);
});

it('can add single tag to the list by clicking button', () => {
    const wrapper = mount(<FormTagCloud value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect( wrapper.find(Badge).length ).toBe(1);
});

it('cannot add a tag if there is a 0 length string', () => {
    const wrapper = mount(<FormTagCloud value="" />);
    wrapper.find(Button).simulate('click');
    expect( wrapper.find(Badge).length ).toBe(0);
});

it('can add single tag to the existing list', () => {
    const wrapper = mount(<FormTagCloud tags={['test', 'me', 'please']} value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect( wrapper.find(Badge).length ).toBe(4);
});

it('can add single tag to the list by pressing enter', () => {
    const wrapper = shallow(<FormTagCloud value="test-tag" />);
    wrapper.find(FormInput).simulate('keyPress', 'test-tag', { key: 'Enter' });
    const tags = wrapper.state().tags;
    expect( tags.length ).toBe(1);
    expect( tags[0] ).toBe('test-tag');
});

it('can add comma separated tags to the list', () => {
    const wrapper = mount(<FormTagCloud value="this, is, tagging" />);
    wrapper.find(Button).simulate('click');
    expect( wrapper.find(Badge).length ).toBe(3);
    expect( wrapper.find(Badge).at(0).children().at(0).text().trim() ).toBe('this');
    expect( wrapper.find(Badge).at(1).children().at(0).text().trim() ).toBe('is');
    expect( wrapper.find(Badge).at(2).children().at(0).text().trim() ).toBe('tagging');
});

it('adding tags should clear the input', () => {
    const wrapper = shallow(<FormTagCloud value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect( wrapper.state().tagInputValue ).toBe('');
});

it('can click a Badge (tag) to remove it from the list', () => {
    const wrapper = mount(<FormTagCloud tags={['remove', 'me', 'please']} />);
    wrapper.find(Badge).at(0).simulate('click');
    expect( wrapper.find(Badge).length ).toBe(2);
});

it('calls observer when tags removed', () => {
    const onChangeMock = jest.fn();
    const wrapper = mount(<FormTagCloud onChange={onChangeMock} tags={['remove', 'me', 'please']} />);
    wrapper.find(Badge).at(0).simulate('click');
    expect(onChangeMock.mock.calls.length).toBe(1);
});

it('calls observer with event data when tags removed', () => {
    const onChangeMock = (tags, event) => {
        expect(tags.length).toBe(2);
        expect(event.type).toBe('click');
    };
    const wrapper = mount(<FormTagCloud onChange={onChangeMock} tags={['remove', 'me', 'please']} />);
    wrapper.find(Badge).at(0).simulate('click');
    expect.hasAssertions();
});

it('calls observer when tags added', () => {
    const onChangeMock = jest.fn();
    const wrapper = mount(<FormTagCloud onChange={onChangeMock} value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect(onChangeMock.mock.calls.length).toBe(1);
});

it('calls observer with event data when tags added', () => {
    const onChangeMock = (tags, event) => {
        expect(tags.length).toBe(1);
        expect(event.type).toBe('click');
    };
    const wrapper = mount(<FormTagCloud onChange={onChangeMock} value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect.hasAssertions();
});

it('can call multiple observers when tags added', () => {
    const onChangeMock = [jest.fn(), jest.fn()];
    const wrapper = shallow(<FormTagCloud onChange={onChangeMock} value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect(onChangeMock[0].mock.calls.length).toBe(1);
    expect(onChangeMock[1].mock.calls.length).toBe(1);
});

it('can assign a label to child FormInput', () => {
    const label = "Skills";
    const wrapper = mount(<FormTagCloud label={label} />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('label', label);
});

it('can assign a placeholder to child FormInput', () => {
    const placeholder = "this is an example placeholder";
    const wrapper = mount(<FormTagCloud placeholder={placeholder} />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('placeholder', placeholder);
});

it('valid state is mapped to FormInput props', () => {
    const wrapper = shallow(<FormTagCloud valid={false} />);
    expect( wrapper.find(FormInput).props() ).toHaveProperty('valid', false);
});

it('passes a validation function through to onValidate', () => {
    const onValidate = jest.fn();
    const wrapper = shallow(<FormTagCloud onValidate={onValidate} value="test-tag" />);
    wrapper.find(Button).simulate('click');
    expect(onValidate.mock.calls.length).toBe(1);
});

it('validateTags sets state based on onVaildate function', () => {
    const onValidate = jest.fn().mockReturnValue(true);
    const wrapper = shallow(<FormTagCloud onValidate={onValidate} />);
    wrapper.instance().validateTags();
    expect(wrapper.state().valid).toBe(true);
});
