import React from 'react';
import { shallow } from 'enzyme';
import { Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import TagCloud from '../components/TagCloud';
import ProfileCard from './ProfileCard';
import { offerBannerCodes } from './ProfileCard';

const exampleData = {
    name: "Matthew Davies",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos sequi esse incidunt doloremque, quod in atque quibusdam, ullam sed quo.",
    image: "http://via.placeholder.com/700x500",
    callouts: ['5 Star Rating'],
    tags: ['developer', 'javascript', 'node'],
    action: "Action This"
};

function createProfileCard( offerBannerCode ) {
    return shallow(<ProfileCard
        title={exampleData.name}
        description={exampleData.description}
        image={exampleData.image}
        callouts={exampleData.callouts}
        tags={exampleData.tags}
        action={exampleData.action}
        banner={offerBannerCode}
    />);
}

it('displays the correct elements', () => {
    const wrapper = createProfileCard();
    expect( wrapper.find('div').at(0).hasClass('profile-card') ).toBe(true);
    expect( wrapper.find(Card).length ).toBe(1);
    expect( wrapper.find(CardImg).length ).toBe(1);
    expect( wrapper.find(CardBody).length ).toBe(1);
    expect( wrapper.find(CardTitle).length ).toBe(1);
    expect( wrapper.find(CardText).length ).toBe(1);
    expect( wrapper.find(CardSubtitle).length ).toBe(1);
    expect( wrapper.find(TagCloud).length ).toBe(1);
    expect( wrapper.find(FontAwesome).length ).toBe(1);
    expect( wrapper.find(Button).length ).toBe(1);
});

it('displays the correct data', () => {
    const wrapper = createProfileCard();
    const shortDescription = wrapper.instance().shortenDescription(exampleData.description, 100);
    expect( wrapper.find(CardImg).at(0).props() ).toHaveProperty('src', exampleData.image);
    expect( wrapper.find(CardTitle).at(0).children().text() ).toBe(exampleData.name);
    expect( wrapper.find(CardText).at(0).children().text() ).toBe(shortDescription);
    expect( wrapper.find(CardSubtitle).at(0).children().text() ).toBe(exampleData.callouts[0]);
    expect( wrapper.find(TagCloud).props() ).toHaveProperty('tags', exampleData.tags);
    expect( wrapper.find(Button).at(0).children().text() ).toBe(exampleData.action);
});

it('can toggle the description length', () => {
    const wrapper = createProfileCard();
    expect( wrapper.state() ).toHaveProperty('shortDescription', true);
    wrapper.find(FontAwesome).simulate('click');
    expect( wrapper.state() ).toHaveProperty('shortDescription', false);
    wrapper.find(FontAwesome).simulate('click');
    expect( wrapper.state() ).toHaveProperty('shortDescription', true);
});

it('calls shorten description when state requires', () => {
    const wrapper = createProfileCard();
    let instance = wrapper.instance();
    instance.shortenDescription = jest.fn();
    instance.setState({ shortDescription: true });
    expect(instance.shortenDescription.mock.calls.length).toBe(1);
});

it('does not call shorten description when state does not require', () => {
    const wrapper = createProfileCard();
    let instance = wrapper.instance();
    instance.shortenDescription = jest.fn();
    instance.setState({ shortDescription: false });
    expect(instance.shortenDescription.mock.calls.length).toBe(0);
});

it('does not call shorten description when state does not require', () => {
    const wrapper = createProfileCard();
    let instance = wrapper.instance();
    expect(instance.shortenDescription('word', 1)).toBe('w...');
});

it('can display the offer sent banner', () => {
    const wrapper = createProfileCard( offerBannerCodes.sent );
    const banner = wrapper.find('div');
    expect( banner.at(1).hasClass('offer-banner offer-banner-sent') ).toBe(true);
    expect( banner.at(1).children().text() ).toBe('Offer Sent');
});

it('can display the offer accepted banner', () => {
    const wrapper = createProfileCard( offerBannerCodes.accepted );
    const banner = wrapper.find('div');
    expect( banner.at(1).hasClass('offer-banner offer-banner-accepted') ).toBe(true);
    expect( banner.at(1).children().text() ).toBe('Offer Accepted');
});

it('can display the offer rejected banner', () => {
    const wrapper = createProfileCard( offerBannerCodes.rejected );
    const banner = wrapper.find('div');
    expect( banner.at(1).hasClass('offer-banner offer-banner-rejected') ).toBe(true);
    expect( banner.at(1).children().text() ).toBe('Offer Rejected');
});
