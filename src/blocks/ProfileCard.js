import React, { Component } from 'react';
import { Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import TagCloud from '../components/TagCloud';
import './profile-card.css';

const DESCRIPTION_LENGTH = 100;

const offerBannerCodes = {
    sent: 100,
    accepted: 200,
    rejected: 300
};

export { offerBannerCodes };

class ProfileCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            shortDescription: true
        };
        this.toggleDescriptionLength = this.toggleDescriptionLength.bind(this);
    }

    getDescription() {
        return this.state.shortDescription ? this.shortenDescription(this.props.description, DESCRIPTION_LENGTH) : this.props.description;
    }

    shortenDescription(description, length) {
        if ( description && description.length > length ) {
            description = description.substr(0, length) + '...';
        }
        return description;
    }

    toggleDescriptionLength() {
        this.setState({
            shortDescription: !this.state.shortDescription
        });
    }

    render() {
        let description = this.getDescription(),
            action = this.props.action ? this.props.action : 'Ask for Help',
            callouts, banner;
        if ( this.props.callouts ) {
            callouts = this.props.callouts.map((callout, index) => <CardSubtitle key={index}>{callout}</CardSubtitle>);
        }
        if ( this.props.banner ) {
            switch ( this.props.banner ) {
                case offerBannerCodes.sent:
                    banner = <div className="offer-banner offer-banner-sent">Offer Sent</div>
                    break;
                case offerBannerCodes.accepted:
                    banner = <div className="offer-banner offer-banner-accepted">Offer Accepted</div>
                    break;
                case offerBannerCodes.rejected:
                    banner = <div className="offer-banner offer-banner-rejected">Offer Rejected</div>
                    break;
                default:
                    break;
            }
        }
        return (
            <div className="profile-card">
                <Card>
                    {banner}
                    <CardImg top src={this.props.image} alt="Profile" />
                    <CardBody>
                        <CardTitle>{this.props.title}</CardTitle>
                        <CardText>{description}</CardText>
                        <FontAwesome name='ellipsis-h' size='lg' onClick={this.toggleDescriptionLength} />
                        {callouts}
                        <TagCloud tags={this.props.tags} />
                        <Button>{action}</Button>
                    </CardBody>
                </Card>
            </div>
        );
    }

}

export default ProfileCard;
