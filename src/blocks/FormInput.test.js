import React from 'react';
import { shallow } from 'enzyme';
import { FormGroup, Label, Input, FormText } from 'reactstrap';
import FormInput from './FormInput';

it('displays form group', () => {
    const wrapper = shallow(<FormInput />);
    expect( wrapper.find(FormGroup).length ).toBe(1);
});

it('displays label', () => {
    const wrapper = shallow(<FormInput label="My Label" id="my-id" />);
    expect.assertions(2);
    expect( wrapper.find(Label).length ).toBe(1);
    expect( wrapper.find(Label).children().text() ).toBe('My Label');
});

it('does not display label if prop is missing', () => {
    const wrapper = shallow(<FormInput />);
    expect( wrapper.find(Label).length ).toBe(0);
});

it('throws error if label is used without id', () => {
    expect( () => { shallow(<FormInput label="My Label" />); } ).toThrow('Define FormInput.props.id to ensure Input and Label are linked');
});

it('displays input element', () => {
    const wrapper = shallow(<FormInput />);
    expect( wrapper.find(Input).length ).toBe(1);
});

it('displays input element with an id, without a label', () => {
    const id = "my-unique-id";
    const wrapper = shallow(<FormInput id={id} />);
    expect( wrapper.find(Input).props() ).toHaveProperty('id', id);
});

it('label and input are connected by for and id', () => {
    const id = "my-unique-id";
    const wrapper = shallow(<FormInput label="My Label" id={id} />);
    expect.assertions(2);
    expect( wrapper.find(Label).props() ).toHaveProperty('for', id);
    expect( wrapper.find(Input).props() ).toHaveProperty('id', id);
});

it('default input type is text', () => {
    const wrapper = shallow(<FormInput />);
    expect( wrapper.find(Input).props() ).toHaveProperty('type', 'text');
});

it('input type can be set', () => {
    const wrapper = shallow(<FormInput type="email" />);
    expect( wrapper.find(Input).props() ).toHaveProperty('type', 'email');
});

it('input placeholder can be set', () => {
    const placeholder = "This is my placeholder";
    const wrapper = shallow(<FormInput placeholder={placeholder} />);
    expect( wrapper.find(Input).props() ).toHaveProperty('placeholder', placeholder);
});

it('help text can be passed in', () => {
    const helpText = 'This is some help text';
    const wrapper = shallow(<FormInput>{helpText}</FormInput>);
    expect( wrapper.find(FormText).children().text() ).toBe(helpText);
});

it('can mark input as valid', () => {
    const wrapper = shallow(<FormInput valid={true} />);
    expect( wrapper.find(Input).props() ).toHaveProperty('valid', true);
});

it('can mark input as invalid', () => {
    const wrapper = shallow(<FormInput valid={false} />);
    expect( wrapper.find(Input).props() ).toHaveProperty('valid', false);
});

it('label has text-danger class when input is invalid', () => {
    const wrapper = shallow(<FormInput label="My Label" id="my-id" valid={false} />);
    expect( wrapper.find(Label).hasClass('text-danger') ).toBe(true);
});

it('can explicitly hide input', () => {
    const wrapper = shallow(<FormInput showInput={false} />);
    expect( wrapper.find(Input).length ).toBe(0);
});

it('allows input value to be set', () => {
    const inputValue = 'value test';
    const wrapper = shallow(<FormInput value={inputValue} />);
    expect( wrapper.find(Input).props() ).toHaveProperty('value', inputValue);
});

it('calls onChange callback when change is triggered', () => {
    const onChangeMock = jest.fn();
    const wrapper = shallow(<FormInput onChange={onChangeMock} />);
    wrapper.find(Input).simulate('change', { target: { value: 'test' } });
    expect(onChangeMock.mock.calls.length).toBe(1);
});

it('can call multiple onChange callbacks when change is triggered', () => {
    const onChangeMock = [jest.fn(), jest.fn()];
    const wrapper = shallow(<FormInput onChange={onChangeMock} />);
    wrapper.find(Input).simulate('change', { target: { value: 'test' } });
    expect(onChangeMock[0].mock.calls.length).toBe(1);
    expect(onChangeMock[1].mock.calls.length).toBe(1);
});

it('calls onKeyPress callback when keypress is triggered', () => {
    const onKeyPressMock = jest.fn();
    const wrapper = shallow(<FormInput onKeyPress={onKeyPressMock} />);
    wrapper.find(Input).simulate('keyPress', { target: { value: '' } });
    expect(onKeyPressMock.mock.calls.length).toBe(1);
});

it('can call multiple onKeyPress callbacks when keypress is triggered', () => {
    const onKeyPressMock = [jest.fn(), jest.fn()];
    const wrapper = shallow(<FormInput onKeyPress={onKeyPressMock} />);
    wrapper.find(Input).simulate('keyPress', { target: { value: '' } });
    expect(onKeyPressMock[0].mock.calls.length).toBe(1);
    expect(onKeyPressMock[1].mock.calls.length).toBe(1);
});

it('passes event object to change hanlder', () => {
    const inputValue = 'my unique value';
    const wrapper = shallow(<FormInput onChange={value => expect(value).toBe(inputValue)} />);
    wrapper.find(Input).simulate('change', { target: { value: inputValue } });
    expect.hasAssertions();
});

it('calls validation function on Input blur', () => {
    const inputValue = 301;
    const onValidate = jest.fn();
    const wrapper = shallow(<FormInput onValidate={onValidate} />);
    wrapper.find(Input).simulate('blur', { target: { value: inputValue } });
    expect( onValidate.mock.calls.length ).toBe(1);
});

it('sets state valid to false if validation returns false', () => {
    const inputValue = 301;
    const onValidate = jest.fn();
    onValidate.mockReturnValue(false);
    const wrapper = shallow(<FormInput onValidate={onValidate} />);
    wrapper.find(Input).simulate('blur', { target: { value: inputValue } });
    expect( wrapper.state().valid ).toBe(false);
});

it('sets state valid to true if validation returns true', () => {
    const inputValue = 301;
    const onValidate = jest.fn();
    onValidate.mockReturnValue(true);
    const wrapper = shallow(<FormInput onValidate={onValidate} />);
    wrapper.find(Input).simulate('blur', { target: { value: inputValue } });
    expect( wrapper.state().valid ).toBe(true);
});

it('passes value through to the validation function', () => {
    const inputValue = 301;
    const onValidate = value => expect(value).toBe(inputValue);
    const wrapper = shallow(<FormInput value={inputValue} onValidate={onValidate} />);
    wrapper.find(Input).simulate('blur', { target: { value: inputValue } });
    expect.hasAssertions();
});
