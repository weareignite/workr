import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import { FormGroup, Label } from 'reactstrap';
import DatePicker from 'react-datepicker';
import FormDateRange from './FormDateRange';

it('displays div with correct class', () => {
    const wrapper = shallow(<FormDateRange />);
    expect( wrapper.find('div').at(0).hasClass('form-date-range') ).toBe(true);
});

it('displays FormGroup', () => {
    const wrapper = shallow(<FormDateRange />);
    expect( wrapper.find(FormGroup).length ).toBe(1);
});

it('displays two div.col', () => {
    const wrapper = shallow(<FormDateRange />);
    expect( wrapper.find('div.col').length ).toBe(2);
});

it('displays two Label', () => {
    const wrapper = shallow(<FormDateRange />);
    expect( wrapper.find(Label).length ).toBe(2);
});

it('displays two DatePicker', () => {
    const wrapper = shallow(<FormDateRange />);
    expect( wrapper.find(DatePicker).length ).toBe(2);
});

it('gives labels default values', () => {
    const wrapper = shallow(<FormDateRange />);
    expect( wrapper.find(Label).at(0).children().text() ).toBe('Start');
    expect( wrapper.find(Label).at(1).children().text() ).toBe('Finish');
});

it('passes values to labels', () => {
    const wrapper = shallow(<FormDateRange startLabel="One" finishLabel="Two" />);
    expect( wrapper.find(Label).at(0).children().text() ).toBe('One');
    expect( wrapper.find(Label).at(1).children().text() ).toBe('Two');
});

it('Datepicker elements have the correct props', () => {
    const date = moment('2017-11-11');
    const wrapper = shallow(<FormDateRange startDate={date} finishDate={date} />);
    const instance = wrapper.instance();
    const datepickerA = wrapper.find(DatePicker).at(0).props();
    expect( datepickerA ).toHaveProperty('selected', date);
    expect( datepickerA ).toHaveProperty('selectsStart', true);
    expect( datepickerA ).toHaveProperty('minDate');
    expect( datepickerA ).toHaveProperty('maxDate');
    expect( datepickerA ).toHaveProperty('startDate', date);
    expect( datepickerA ).toHaveProperty('endDate', date);
    expect( datepickerA ).toHaveProperty('onChange', instance.handleStartChange);
    expect( datepickerA ).toHaveProperty('className', "form-control");
    const datepickerB = wrapper.find(DatePicker).at(1).props();
    expect( datepickerB ).toHaveProperty('selected', date);
    expect( datepickerB ).toHaveProperty('selectsEnd', true);
    expect( datepickerB ).toHaveProperty('minDate');
    expect( datepickerB ).toHaveProperty('maxDate');
    expect( datepickerB ).toHaveProperty('startDate', date);
    expect( datepickerB ).toHaveProperty('endDate', date);
    expect( datepickerB ).toHaveProperty('onChange', instance.handleFinishChange);
    expect( datepickerB ).toHaveProperty('className', "form-control");
});

it('calls handleChange when handleStartChange is called', () => {
    const wrapper = shallow(<FormDateRange />);
    const instance = wrapper.instance();
    instance.handleChange = jest.fn();
    instance.handleStartChange( moment() );
    expect( instance.handleChange.mock.calls.length ).toBe(1);
});

it('calls handleChange when handleFinishChange is called', () => {
    const wrapper = shallow(<FormDateRange />);
    const instance = wrapper.instance();
    instance.handleChange = jest.fn();
    instance.handleFinishChange( moment() );
    expect( instance.handleChange.mock.calls.length ).toBe(1);
});

it('handleStartChange sets startDate', () => {
    const wrapper = shallow(<FormDateRange />);
    const instance = wrapper.instance();
    const date = moment('2017-11-11');
    instance.handleStartChange( date );
    expect( wrapper.state().startDate.isSame(date) ).toBe(true);
});

it('handleStartChange sets finishDate if before new date', () => {
    const wrapper = shallow(<FormDateRange startDate={moment('2017-11-01')} finishDate={moment('2017-11-30')} />);
    const instance = wrapper.instance();
    const date = moment('2017-12-12');
    instance.handleStartChange( date );
    expect( wrapper.state().startDate.isSame(date) ).toBe(true);
    expect( wrapper.state().finishDate.isSame(date) ).toBe(true);
});

it('handleFinishChange sets finishDate', () => {
    const wrapper = shallow(<FormDateRange />);
    const instance = wrapper.instance();
    const date = moment('2017-11-11');
    instance.handleFinishChange( date );
    expect( wrapper.state().finishDate.isSame(date) ).toBe(true);
});

it('handleFinishChange sets startDate if after new date', () => {
    const wrapper = shallow(<FormDateRange startDate={moment('2017-11-01')} finishDate={moment('2017-11-30')} />);
    const instance = wrapper.instance();
    const date = moment('2017-10-10');
    instance.handleFinishChange( date );
    expect( wrapper.state().startDate.isSame(date) ).toBe(true);
    expect( wrapper.state().finishDate.isSame(date) ).toBe(true);
});

it('calls onChange callback when change is triggered', () => {
    const onChangeMock = jest.fn();
    const wrapper = shallow(<FormDateRange onChange={onChangeMock} />);
    wrapper.instance().handleChange();
    expect(onChangeMock.mock.calls.length).toBe(1);
});

it('can call multiple onChange callbacks when change is triggered', () => {
    const onChangeMock = [jest.fn(), jest.fn()];
    const wrapper = shallow(<FormDateRange onChange={onChangeMock} />);
    wrapper.instance().handleChange();
    expect(onChangeMock[0].mock.calls.length).toBe(1);
    expect(onChangeMock[1].mock.calls.length).toBe(1);
});

it('gives label text-danger class when prop valid is false', () => {
    const wrapper = shallow(<FormDateRange valid={false} />);
    expect( wrapper.find(Label).at(0).hasClass('text-danger') ).toBe(true);
    expect( wrapper.find(Label).at(1).hasClass('text-danger') ).toBe(true);
});

it('calls validate function on handleChange', () => {
    const wrapper = shallow(<FormDateRange />);
    const instance = wrapper.instance();
    instance.validate = jest.fn();
    instance.handleChange();
    expect( instance.validate.mock.calls.length ).toBe(1);
});

it('calls props onValidate when validate function is called', () => {
    const onValidate = jest.fn().mockReturnValue(true);
    const wrapper = shallow(<FormDateRange onValidate={onValidate} />);
    const instance = wrapper.instance();
    instance.validate();
    expect( onValidate.mock.calls.length ).toBe(1);
});

it('sets valid state true when both onValidate functions return true', () => {
    const onValidate = jest.fn().mockReturnValue(true);
    const wrapper = shallow(<FormDateRange onValidate={onValidate} />);
    const instance = wrapper.instance();
    instance.validate();
    expect( wrapper.state().valid ).toBe(true);
});

it('sets valid state false when either onValidate functions returns false', () => {
    const onValidate = jest.fn().mockReturnValueOnce(false).mockReturnValue(true);
    const wrapper = shallow(<FormDateRange onValidate={onValidate} />);
    const instance = wrapper.instance();
    instance.validate();
    expect( wrapper.state().valid ).toBe(false);
});
