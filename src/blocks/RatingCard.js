import React, { Component } from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import StarRating from '../components/StarRating';

class RatingCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: props.open ? props.open : false
        };
        this.handleClose = this.handleClose.bind(this);
    }

    handleClose() {
        this.setState({ open: !this.state.open });
    }

    render() {
        return (
            <Modal size="sm" isOpen={this.state.open} className="rating-card">
                <ModalHeader toggle={this.handleClose}>Rate {this.props.name}</ModalHeader>
                <ModalBody>
                    <img src={this.props.image} className="img-fluid" alt="Profile" />
                    <StarRating onChange={this.props.onChange} />
                </ModalBody>
            </Modal>
        );
    }
}

export default RatingCard;
