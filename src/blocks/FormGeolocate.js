import React, { Component } from 'react';
import FormInput from './FormInput';
import GoogleMapStatic from '../components/GoogleMapStatic';
import debounce from 'lodash.debounce';

class FormGeolocate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: props.value ? props.value : '',
            valid: null
        };
        this.timeoutSpeed = typeof props.timeout === 'undefined' || Number.isNaN(props.timeout) ? 600 : props.timeout;
        this.handleChange = this.handleChange.bind(this);
    }

    debounce(action, timeout, event) {
        this.debouncedFunction = debounce(action, timeout);
        return this.debouncedFunction(event);
    }

    cancelDebounce() {
        if ( typeof this.debouncedFunction !== 'undefined' ) {
            return this.debouncedFunction.cancel();
        }
    }

    handleChange(value, event) {
        const changeHandler = value => {
            this.setState({ value: value });
        };
        if ( this.timeoutSpeed < 1 ) {
            changeHandler(value);
        } else {
            // @NB: taken from https://reactjs.org/docs/events.html#event-pooling
            event.persist();
            this.cancelDebounce();
            this.debounce(changeHandler, this.timeoutSpeed, value);
        }
    }

    render () {
        return (
            <div className="form-geolocate">
                <FormInput
                    label={this.props.label}
                    id={this.props.id}
                    value={this.props.value}
                    valid={typeof this.props.valid === 'undefined' ? this.state.valid : this.props.valid}
                    onValidate={this.props.onValidate}
                    placeholder={this.props.placeholder}
                    onChange={this.handleChange}>
                        {this.props.children}
                </FormInput>
                <GoogleMapStatic address={this.state.value} />
            </div>
        );
    }

}

export default FormGeolocate;
