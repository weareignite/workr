import React, { Component } from 'react';
import moment from 'moment';
import { FormGroup, Label } from 'reactstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class FormDateRange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valid: null,
            startDate: props.startDate ? props.startDate : moment(),
            finishDate: props.finishDate ? props.finishDate : moment()
        };
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleFinishChange = this.handleFinishChange.bind(this);
    }

    handleChange() {
        if ( this.props.onChange ) {
            const onChangeObservers = Array.isArray(this.props.onChange) ? this.props.onChange : [this.props.onChange];
            onChangeObservers.forEach(onChange => onChange({ start: this.state.startDate, finish: this.state.finishDate }));
        }
        this.validate();
    }

    handleStartChange(date) {
        let state = { startDate: date };
        if ( this.state.finishDate.isBefore(date) ) {
            state.finishDate = date;
        }
        this.setState(state);
        this.handleChange(date);
    }

    handleFinishChange(date) {
        let state = { finishDate: date };
        if ( this.state.startDate.isAfter(date) ) {
            state.startDate = date;
        }
        this.setState(state);
        this.handleChange(date);
    }

    validate() {
        if ( this.props.onValidate ) {
            this.setState({
                valid: this.props.onValidate({ start: this.state.startDate, finish: this.state.finishDate })
            });
        }
    }

    render() {
        let valid = typeof this.props.valid === 'undefined' ? this.state.valid : this.props.valid,
            labelClass = valid === false ? 'text-danger' : null,
            startLabel = this.props.startLabel ? this.props.startLabel : 'Start',
            finishLabel = this.props.finishLabel ? this.props.finishLabel : 'Finish';
        return (
            <div className="form-date-range">
                <FormGroup className="row">
                    <div className="col">
                        <Label className={labelClass}>{startLabel}</Label>
                        <DatePicker
                            selected={this.state.startDate}
                            selectsStart
                            minDate={this.props.startMinDate}
                            maxDate={this.props.startMaxDate}
                            startDate={this.state.startDate}
                            endDate={this.state.finishDate}
                            onChange={this.handleStartChange}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <Label className={labelClass}>{finishLabel}</Label>
                        <DatePicker
                            selected={this.state.finishDate}
                            selectsEnd
                            minDate={this.props.finishMinDate}
                            maxDate={this.props.finishMaxDate}
                            startDate={this.state.startDate}
                            endDate={this.state.finishDate}
                            onChange={this.handleFinishChange}
                            className="form-control"
                        />
                    </div>
                </FormGroup>
            </div>
        );
    }
}

export default FormDateRange;
