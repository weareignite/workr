import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { FormGroup, Label } from 'reactstrap';
import Slider from 'rc-slider';
import FormSlider from './FormSlider';

it('displays a div with correct class', () => {
    const wrapper = shallow(<FormSlider />);
    expect( wrapper.find('div').at(0).hasClass('form-slider') ).toBe(true);
});

it('displays a FormGroup', () => {
    const wrapper = shallow(<FormSlider />);
    expect( wrapper.find(FormGroup).length ).toBe(1);
});

it('displays a Slider', () => {
    const wrapper = shallow(<FormSlider />);
    expect( wrapper.find(Slider).length ).toBe(1);
});

it('can pass minimum value to slider, which also sets default value', () => {
    const min = 10;
    const wrapper = shallow(<FormSlider min={min} />);
    expect( wrapper.find(Slider).props() ).toMatchObject({
        min: min,
        defaultValue: min
    });
});

it('can pass maximum value to slider', () => {
    const max = 57;
    const wrapper = shallow(<FormSlider max={max} />);
    expect( wrapper.find(Slider).props() ).toMatchObject({
        max: max
    });
});

it('can pass value to slider', () => {
    const value = 34;
    const wrapper = shallow(<FormSlider value={value} />);
    expect( wrapper.find(Slider).props() ).toMatchObject({
        defaultValue: value
    });
});

it('will show value in span', () => {
    const value = 34;
    const wrapper = shallow(<FormSlider value={value} />);
    expect( wrapper.find('span.pull-right').length ).toBe(1);
    expect( wrapper.find('span.pull-right').children().text() ).toBe('34');
});

it('can format shown value', () => {
    const value = 34;
    const valueFormat = "£%d";
    const wrapper = shallow(<FormSlider value={value} valueFormat={valueFormat} />);
    expect( wrapper.find('span.pull-right').children().text() ).toBe('£34');
});

it('can have show a label', () => {
    const label = "My Slider";
    const wrapper = shallow(<FormSlider label={label} />);
    expect( wrapper.find(Label).length ).toBe(1);
    expect( wrapper.find(Label).children().text() ).toBe(label);
});

it('can update state by adjusting handleChange', () => {
    expect.hasAssertions();
    const value = 202;
    const wrapper = shallow(<FormSlider />);
    const instance = wrapper.instance();
    instance.handleChange(value);
    expect( wrapper.state().value ).toBe(value);
});

it('calls onChange callback when change is triggered', () => {
    const onChangeMock = jest.fn();
    const wrapper = shallow(<FormSlider onChange={onChangeMock} />);
    wrapper.find(Slider).simulate('afterChange', 10);
    expect(onChangeMock.mock.calls.length).toBe(1);
});

it('can call multiple onChange callbacks when change is triggered', () => {
    const onChangeMock = [jest.fn(), jest.fn()];
    const wrapper = shallow(<FormSlider onChange={onChangeMock} />);
    wrapper.find(Slider).simulate('afterChange', 20);
    expect(onChangeMock[0].mock.calls.length).toBe(1);
    expect(onChangeMock[1].mock.calls.length).toBe(1);
});

it('gives label text-danger class when prop valid is false', () => {
    const wrapper = shallow(<FormSlider label="My Label" valid={false} />);
    expect( wrapper.find(Label).hasClass('text-danger') ).toBe(true);
});

it('calls validate function on afterChange', () => {
    const wrapper = shallow(<FormSlider />);
    const instance = wrapper.instance();
    instance.validate = jest.fn();
    wrapper.find(Slider).simulate('afterChange');
    expect( instance.validate.mock.calls.length ).toBe(1);
});

it('calls props onValidate when validate function is called', () => {
    const onValidate = jest.fn();
    const wrapper = shallow(<FormSlider onValidate={onValidate} />);
    const instance = wrapper.instance();
    instance.validate()
    expect( onValidate.mock.calls.length ).toBe(1);
});

it('sets valid state when validate function is called', () => {
    const onValidate = jest.fn().mockReturnValue(true);
    const wrapper = shallow(<FormSlider onValidate={onValidate} />);
    const instance = wrapper.instance();
    instance.validate()
    expect( wrapper.state().valid ).toBe(true);
});
