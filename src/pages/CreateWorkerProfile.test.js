import React from 'react';
import { shallow } from 'enzyme';
import FormWorkerProfile from '../sections/FormWorkerProfile';
import CreateWorkerProfile from './CreateWorkerProfile';

it('displays base element with correct class', () => {
    const wrapper = shallow(<CreateWorkerProfile />);
    expect( wrapper.find('div').at(0).hasClass('create-worker-profile') ).toBe(true);
});

it('displays h1 with correct title', () => {
    const wrapper = shallow(<CreateWorkerProfile />);
    expect( wrapper.find('h1').at(0).children().text() ).toBe('Tell us what you can do!');
});

it('displays FormWorkerProfile', () => {
    const wrapper = shallow(<CreateWorkerProfile />);
    expect( wrapper.find(FormWorkerProfile).length ).toBe(1);
    expect( wrapper.find(FormWorkerProfile).props() ).toHaveProperty('actionText', 'Find Jobs');
});

it('displays existing account login link', () => {
    const wrapper = shallow(<CreateWorkerProfile />);
    expect( wrapper.find('a').at(0).children().text() ).toBe('login to your existing account');
});
