import React from 'react';
import { Button } from 'reactstrap';
import FormCreateAccount from '../sections/FormCreateAccount';

function CreateWorkerProfile() {
    return (
        <div className="create-account">
            <h1>Almost done!</h1>
            <h2>Please create an account</h2>
            <FormCreateAccount />
            <p className="text-center">
                <a href="">already have an account?</a>
            </p>
        </div>
    );
}

export default CreateWorkerProfile;
