import React from 'react';
import { shallow } from 'enzyme';
import FormForgotPassword from '../sections/FormForgotPassword';
import ForgotPassword from './ForgotPassword';

it('displays base element with correct class', () => {
    const wrapper = shallow(<ForgotPassword />);
    expect( wrapper.find('div').at(0).hasClass('forgot-password') ).toBe(true);
});

it('displays h1 with correct title', () => {
    const wrapper = shallow(<ForgotPassword />);
    expect( wrapper.find('h1').at(0).children().text() ).toBe('Forgot your password?');
});

it('displays FormForgotPassword', () => {
    const wrapper = shallow(<ForgotPassword />);
    expect( wrapper.find(FormForgotPassword).length ).toBe(1);
});

it('displays back to login link', () => {
    const wrapper = shallow(<ForgotPassword />);
    expect( wrapper.find('a').at(0).children().text() ).toBe('back to login');
});
