import React from 'react';
import { Button } from 'reactstrap';

function Home() {
    return (
        <div className="home">
            <h1 className="display-1">Connecting Workers and Employers</h1>
            <Button>find a gig</Button>
            <Button>find a worker</Button>
        </div>
    );
}

export default Home;
