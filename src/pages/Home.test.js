import React from 'react';
import { shallow } from 'enzyme';
import { Button } from 'reactstrap';
import Home from './Home';

it('displays base element with correct class', () => {
    const wrapper = shallow(<Home />);
    expect( wrapper.find('div').at(0).hasClass('home') ).toBe(true);
});

it('displays h1 with correct title and class', () => {
    const wrapper = shallow(<Home />);
    const h1 = wrapper.find('h1').at(0);
    expect( h1.children().text() ).toBe('Connecting Workers and Employers');
    expect( h1.hasClass('display-1') ).toBe(true);
});

it('displays find a gig button', () => {
    const wrapper = shallow(<Home />);
    expect( wrapper.find(Button).at(0).children().text() ).toBe('find a gig');
});

it('displays find a worker button', () => {
    const wrapper = shallow(<Home />);
    expect( wrapper.find(Button).at(1).children().text() ).toBe('find a worker');
});
