import React from 'react';
import FormWorkerProfile from '../sections/FormWorkerProfile';

function CreateWorkerProfile() {
    return (
        <div className="create-worker-profile">
            <h1>Tell us what you can do!</h1>
            <FormWorkerProfile actionText="Find Jobs"/>
            <p className="text-center">
                <a href="">login to your existing account</a>
            </p>
        </div>
    );
}

export default CreateWorkerProfile;
