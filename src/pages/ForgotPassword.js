import React from 'react';
import { Button } from 'reactstrap';
import FormForgotPassword from '../sections/FormForgotPassword';

function ForgotPassword() {
    return (
        <div className="forgot-password">
            <h1>Forgot your password?</h1>
            <FormForgotPassword />
            <p className="text-center">
                <a href="">back to login</a>
            </p>
        </div>
    );
}

export default ForgotPassword;
