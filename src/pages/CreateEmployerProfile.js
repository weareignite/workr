import React from 'react';
import FormEmployerProfile from '../sections/FormEmployerProfile';

function CreateEmployerProfile() {
    return (
        <div className="create-worker-profile">
            <h1>Tell us about the job!</h1>
            <FormEmployerProfile actionText="Find Workers"/>
        </div>
    );
}

export default CreateEmployerProfile;
