import React from 'react';
import { shallow } from 'enzyme';
import FormCreateAccount from '../sections/FormCreateAccount';
import CreateAccount from './CreateAccount';

it('displays base element with correct class', () => {
    const wrapper = shallow(<CreateAccount />);
    expect( wrapper.find('div').at(0).hasClass('create-account') ).toBe(true);
});

it('displays h1 with correct title', () => {
    const wrapper = shallow(<CreateAccount />);
    expect( wrapper.find('h1').at(0).children().text() ).toBe('Almost done!');
});

it('displays h2 with correct title', () => {
    const wrapper = shallow(<CreateAccount />);
    expect( wrapper.find('h2').at(0).children().text() ).toBe('Please create an account');
});

it('displays FormCreateAccount', () => {
    const wrapper = shallow(<CreateAccount />);
    expect( wrapper.find(FormCreateAccount).length ).toBe(1);
});

it('displays existing account login link', () => {
    const wrapper = shallow(<CreateAccount />);
    expect( wrapper.find('a').at(0).children().text() ).toBe('already have an account?');
});
