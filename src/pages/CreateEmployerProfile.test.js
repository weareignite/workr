import React from 'react';
import { shallow } from 'enzyme';
import FormEmployerProfile from '../sections/FormEmployerProfile';
import CreateEmployerProfile from './CreateEmployerProfile';

it('displays base element with correct class', () => {
    const wrapper = shallow(<CreateEmployerProfile />);
    expect( wrapper.find('div').at(0).hasClass('create-worker-profile') ).toBe(true);
});

it('displays h1 with correct title', () => {
    const wrapper = shallow(<CreateEmployerProfile />);
    expect( wrapper.find('h1').at(0).children().text() ).toBe('Tell us about the job!');
});

it('displays FormEmployerProfile', () => {
    const wrapper = shallow(<CreateEmployerProfile />);
    expect( wrapper.find(FormEmployerProfile).length ).toBe(1);
    expect( wrapper.find(FormEmployerProfile).props() ).toHaveProperty('actionText', 'Find Workers');
});
