import React from 'react';
import { shallow, mount } from 'enzyme';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormTagCloud from '../blocks/FormTagCloud';
import FormGeolocate from '../blocks/FormGeolocate';
import FormLogin from './FormLogin';

it('displays base element with correct class', () => {
    const wrapper = shallow(<FormLogin />);
    expect( wrapper.find('div').at(0).hasClass('form-login') ).toBe(true);
});

it('displays h1 with correct title', () => {
    const wrapper = shallow(<FormLogin />);
    expect( wrapper.find('h1').at(0).children().text() ).toBe('Sign into your account');
});

it('displays email FormInput with correct label, id and type', () => {
    const wrapper = shallow(<FormLogin />);
    const props = wrapper.find(FormInput).at(0).props();
    expect( props ).toHaveProperty('label', 'Email');
    expect( props ).toHaveProperty('id', 'email');
    expect( props ).toHaveProperty('type', 'email');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays password FormInput with correct label, id and type', () => {
    const wrapper = shallow(<FormLogin />);
    const props = wrapper.find(FormInput).at(1).props();
    expect( props ).toHaveProperty('label', 'Password');
    expect( props ).toHaveProperty('id', 'password');
    expect( props ).toHaveProperty('type', 'password');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays login Button with correct class and text', () => {
    const wrapper = shallow(<FormLogin />);
    const button = wrapper.find(Button).at(0);
    expect( button.props() ).toHaveProperty('className', 'btn-login btn-action-major');
    expect( button.children().text() ).toBe('Sign In');
});

it('displays forgot password link', () => {
    const wrapper = shallow(<FormLogin />);
    expect( wrapper.find('a').at(0).children().text() ).toBe('forgot your password?');
});

let testSimulateInputChange = (key, index) => {
    const testValue = 'test me';
    const wrapper = mount(<FormLogin />);
    wrapper.find('Input').at(index).simulate('change', { target: { value: testValue }, persist: jest.fn() })
    expect( wrapper.state().values[key] ).toBe(testValue);
}

it('sets state when input email is updated', () => {
    testSimulateInputChange('email', 0);
});

it('sets state when input password is updated', () => {
    testSimulateInputChange('password', 1);
});

it('triggers handleSubmit when Button is clicked if isValid returns true', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormLogin onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => true;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(1);
});

it('does not trigger handleSubmit when Button is clicked if isValid returns false', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormLogin onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => false;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(0);
});

it('isValid returns true if validations are true', () => {
    const wrapper = shallow(<FormLogin />);
    expect( wrapper.instance().isValid({ name: value => true }) ).toBe(true);
});

it('isValid returns false if validations are false', () => {
    const wrapper = shallow(<FormLogin />);
    expect( wrapper.instance().isValid({ name: value => false }) ).toBe(false);
});
