import React from 'react';
import { shallow, mount } from 'enzyme';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormTagCloud from '../blocks/FormTagCloud';
import FormGeolocate from '../blocks/FormGeolocate';
import FormCreateAccount from './FormCreateAccount';

it('displays base element with correct class', () => {
    const wrapper = shallow(<FormCreateAccount />);
    expect( wrapper.find('div').at(0).hasClass('form-create-account') ).toBe(true);
});

it('displays name FormInput with correct label and id', () => {
    const wrapper = shallow(<FormCreateAccount />);
    const props = wrapper.find(FormInput).at(0).props();
    expect( props ).toHaveProperty('label', 'Full Name');
    expect( props ).toHaveProperty('id', 'name');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays email FormInput with correct label and id', () => {
    const wrapper = shallow(<FormCreateAccount />);
    const props = wrapper.find(FormInput).at(1).props();
    expect( props ).toHaveProperty('label', 'Email');
    expect( props ).toHaveProperty('id', 'email');
    expect( props ).toHaveProperty('type', 'email');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays phone FormInput with correct label and id', () => {
    const wrapper = shallow(<FormCreateAccount />);
    const props = wrapper.find(FormInput).at(2).props();
    expect( props ).toHaveProperty('label', 'Mobile Phone');
    expect( props ).toHaveProperty('id', 'phone');
    expect( props ).toHaveProperty('type', 'tel');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays create account Button with correct class and text', () => {
    const wrapper = shallow(<FormCreateAccount />);
    const button = wrapper.find(Button).at(0);
    expect( button.props() ).toHaveProperty('className', 'btn-create-account btn-action-major');
    expect( button.children().text() ).toBe('Create Account');
});

let testSimulateInputChange = (key, index) => {
    const testValue = 'test me';
    const wrapper = mount(<FormCreateAccount />);
    wrapper.find('Input').at(index).simulate('change', { target: { value: testValue }, persist: jest.fn() })
    expect( wrapper.state().values[key] ).toBe(testValue);
}

it('sets state when input name is updated', () => {
    testSimulateInputChange('name', 0);
});

it('sets state when input email is updated', () => {
    testSimulateInputChange('email', 1);
});

it('sets state when input phone is updated', () => {
    testSimulateInputChange('phone', 2);
});

it('triggers handleSubmit when Button is clicked if isValid returns true', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormCreateAccount onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => true;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(1);
});

it('does not trigger handleSubmit when Button is clicked if isValid returns false', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormCreateAccount onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => false;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(0);
});

it('isValid returns true if validations are true', () => {
    const wrapper = shallow(<FormCreateAccount />);
    expect( wrapper.instance().isValid({ name: value => true }) ).toBe(true);
});

it('isValid returns false if validations are false', () => {
    const wrapper = shallow(<FormCreateAccount />);
    expect( wrapper.instance().isValid({ name: value => false }) ).toBe(false);
});
