import React from 'react';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import ValidatedComponent, { validateRequired } from '../Validations';

const _errorMessages = {
    email: 'Please enter your email',
};

const _validationMap = {
    email: validateRequired,
};

class FormForgotPassword extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                email: '',
            }
        };
    }

    render() {
        let errorMessage = key => this.getValidationErrorMessage(this.state.validations, _errorMessages, key);
        return (
            <div className="form-forgot-password">
                <FormInput
                    label="Email"
                    id="email"
                    type="email"
                    placeholder="e.g. josie.smith@mail.com"
                    value={this.state.values.email}
                    valid={this.state.validations.email}
                    onChange={this.handleChange.bind(this, 'email')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'email')}>
                    {errorMessage('email')}
                </FormInput>
                <Button onClick={this.handleSubmit.bind(this, _validationMap)} className="btn-forgot-password btn-action-major">Request Reset</Button>
            </div>
        );
    }
}

export default FormForgotPassword;
