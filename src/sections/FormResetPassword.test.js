import React from 'react';
import { shallow, mount } from 'enzyme';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormTagCloud from '../blocks/FormTagCloud';
import FormGeolocate from '../blocks/FormGeolocate';
import FormResetPassword from './FormResetPassword';

it('displays base element with correct class', () => {
    const wrapper = shallow(<FormResetPassword />);
    expect( wrapper.find('div').at(0).hasClass('form-forgot-password') ).toBe(true);
});

it('displays h1 with correct title', () => {
    const wrapper = shallow(<FormResetPassword />);
    expect( wrapper.find('h1').at(0).children().text() ).toBe('Reset your password');
});

it('displays email FormInput with correct label, id and type', () => {
    const wrapper = shallow(<FormResetPassword />);
    const props = wrapper.find(FormInput).at(0).props();
    expect( props ).toHaveProperty('label', 'Email');
    expect( props ).toHaveProperty('id', 'email');
    expect( props ).toHaveProperty('type', 'email');
});

it('displays password FormInput with correct label, id and type', () => {
    const wrapper = shallow(<FormResetPassword />);
    const props = wrapper.find(FormInput).at(1).props();
    expect( props ).toHaveProperty('label', 'Password');
    expect( props ).toHaveProperty('id', 'password');
    expect( props ).toHaveProperty('type', 'password');
});

it('displays repeat password FormInput with correct label, id and type', () => {
    const wrapper = shallow(<FormResetPassword />);
    const props = wrapper.find(FormInput).at(2).props();
    expect( props ).toHaveProperty('label', 'Repeat password');
    expect( props ).toHaveProperty('id', 'repeatPassword');
    expect( props ).toHaveProperty('type', 'password');
});

it('displays reset password Button with correct class and text', () => {
    const wrapper = shallow(<FormResetPassword />);
    const button = wrapper.find(Button).at(0);
    expect( button.props() ).toHaveProperty('className', 'btn-reset-password btn-action-major');
    expect( button.children().text() ).toBe('Reset Password');
});

it('displays back to login link', () => {
    const wrapper = shallow(<FormResetPassword />);
    expect( wrapper.find('a').at(0).children().text() ).toBe('back to login');
});

let testSimulateInputChange = (key, index) => {
    const testValue = 'test me';
    const wrapper = mount(<FormResetPassword />);
    wrapper.find('Input').at(index).simulate('change', { target: { value: testValue }, persist: jest.fn() })
    expect( wrapper.state().values[key] ).toBe(testValue);
}

it('sets state when input email is updated', () => {
    testSimulateInputChange('email', 0);
});

it('sets state when input email is updated', () => {
    testSimulateInputChange('password', 1);
});

it('sets state when input email is updated', () => {
    testSimulateInputChange('repeatPassword', 2);
});

it('triggers handleSubmit when Button is clicked if isValid returns true', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormResetPassword onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => true;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(1);
});

it('does not trigger handleSubmit when Button is clicked if isValid returns false', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormResetPassword onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => false;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(0);
});

it('isValid returns true if validations are true', () => {
    const wrapper = shallow(<FormResetPassword />);
    expect( wrapper.instance().isValid({ name: value => true }) ).toBe(true);
});

it('isValid returns false if validations are false', () => {
    const wrapper = shallow(<FormResetPassword />);
    expect( wrapper.instance().isValid({ name: value => false }) ).toBe(false);
});
