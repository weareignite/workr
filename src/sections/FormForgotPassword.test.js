import React from 'react';
import { shallow, mount } from 'enzyme';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormTagCloud from '../blocks/FormTagCloud';
import FormGeolocate from '../blocks/FormGeolocate';
import FormForgotPassword from './FormForgotPassword';

it('displays base element with correct class', () => {
    const wrapper = shallow(<FormForgotPassword />);
    expect( wrapper.find('div').at(0).hasClass('form-forgot-password') ).toBe(true);
});

it('displays email FormInput with correct label and id', () => {
    const wrapper = shallow(<FormForgotPassword />);
    const props = wrapper.find(FormInput).at(0).props();
    expect( props ).toHaveProperty('label', 'Email');
    expect( props ).toHaveProperty('id', 'email');
    expect( props ).toHaveProperty('type', 'email');
});

it('displays forgot password Button with correct class and text', () => {
    const wrapper = shallow(<FormForgotPassword />);
    const button = wrapper.find(Button).at(0);
    expect( button.props() ).toHaveProperty('className', 'btn-forgot-password btn-action-major');
    expect( button.children().text() ).toBe('Request Reset');
});

let testSimulateInputChange = (key, index) => {
    const testValue = 'test me';
    const wrapper = mount(<FormForgotPassword />);
    wrapper.find('Input').at(index).simulate('change', { target: { value: testValue }, persist: jest.fn() })
    expect( wrapper.state().values[key] ).toBe(testValue);
}

it('sets state when input email is updated', () => {
    testSimulateInputChange('email', 0);
});

it('triggers handleSubmit when Button is clicked if isValid returns true', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormForgotPassword onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => true;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(1);
});

it('does not trigger handleSubmit when Button is clicked if isValid returns false', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormForgotPassword onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => false;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(0);
});

it('isValid returns true if validations are true', () => {
    const wrapper = shallow(<FormForgotPassword />);
    expect( wrapper.instance().isValid({ name: value => true }) ).toBe(true);
});

it('isValid returns false if validations are false', () => {
    const wrapper = shallow(<FormForgotPassword />);
    expect( wrapper.instance().isValid({ name: value => false }) ).toBe(false);
});
