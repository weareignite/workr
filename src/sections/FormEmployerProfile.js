import React from 'react';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormSlider from '../blocks/FormSlider';
import FormDateRange from '../blocks/FormDateRange';
import FormTagCloud from '../blocks/FormTagCloud';
import ValidatedComponent, { validateRequired, validateRequiredArray, validateNumber, validateDateRange } from '../Validations';
import moment from 'moment';

const _errorMessages = {
    jobTitle: 'Please enter a job title',
    description: 'Please enter a short job description',
    business: 'Please enter your business name',
    address: 'Please enter the job address',
    dateRange: 'Please enter a valid start and finish date',
    skills: 'Please list a couple of required key skills',
};

const _validationMap = {
    jobTitle: validateRequired,
    description: validateRequired,
    business: validateRequired,
    address: validateRequired,
    hourlyRate: validateNumber,
    dateRange: validateDateRange,
    skills: validateRequiredArray,
};

class FormEmployerProfile extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                jobTitle: '',
                description: '',
                business: '',
                address: '',
                hourlyRate: 9,
                dateRange: {
                    start: moment(),
                    finish: moment()
                },
                skills: [],
            },
            actionText: props.actionText ? props.actionText : 'Save'
        };
    }

    render() {
        let errorMessage = key => this.getValidationErrorMessage(this.state.validations, _errorMessages, key);
        return (
            <div className="form-employer-profile">
                <FormInput
                    label="Business Name"
                    id="business"
                    placeholder="e.g. Seven Stars on the Green"
                    value={this.state.values.business}
                    valid={this.state.validations.business}
                    onChange={this.handleChange.bind(this, 'business')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'business')}>
                    {errorMessage('business')}
                </FormInput>
                <FormInput
                    label="Job Title"
                    id="jobTitle"
                    placeholder="e.g. Sous Chef"
                    value={this.state.values.jobTitle}
                    valid={this.state.validations.jobTitle}
                    onChange={this.handleChange.bind(this, 'jobTitle')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'jobTitle')}>
                    {errorMessage('jobTitle')}
                </FormInput>
                <FormInput
                    label="Job Description"
                    id="description"
                    type="textarea"
                    placeholder="e.g. Talented sous chef required for one evening"
                    value={this.state.values.description}
                    valid={this.state.validations.description}
                    onChange={this.handleChange.bind(this, 'description')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'description')}>
                    {errorMessage('description')}
                </FormInput>
                <FormInput
                    label="Address"
                    id="address"
                    type="textarea"
                    placeholder="e.g. The Green, Edinburgh, EH11 6DD"
                    value={this.state.values.address}
                    valid={this.state.validations.address}
                    onChange={this.handleChange.bind(this, 'address')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'address')}>
                    {errorMessage('address')}
                </FormInput>
                <FormSlider
                    label="Hourly Rate"
                    value={this.state.values.hourlyRate}
                    valueFormat="£%d"
                    min={5}
                    max={50}
                    onChange={this.handleChange.bind(this, 'hourlyRate')}
                    onValidate={this.handleValidate.bind(this, 'hourlyRate')} />
                <FormDateRange
                    startMinDate={moment()}
                    finishMinDate={moment()}
                    startDate={this.state.values.dateRange.start}
                    finishDate={this.state.values.dateRange.finish}
                    onChange={this.handleChange.bind(this, 'dateRange')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'dateRange')}>
                    {errorMessage('dateRange')}
                </FormDateRange>
                <FormTagCloud
                    label="Skills"
                    id="skills"
                    placeholder="e.g. creativity, time management"
                    tags={this.state.values.skills}
                    valid={this.state.validations.skills}
                    onChange={this.handleChange.bind(this, 'skills')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'skills')}>
                    {errorMessage('skills')}
                </FormTagCloud>
                <Button onClick={this.handleSubmit.bind(this, _validationMap)} className="btn-action-major">{this.state.actionText}</Button>
            </div>
        );
    }
}

export default FormEmployerProfile;
