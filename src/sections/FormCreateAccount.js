import React from 'react';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import ValidatedComponent, { validateRequired, validateEmail } from '../Validations';

const _errorMessages = {
    name: 'Please enter your full name',
    email: 'Please enter your email',
    phone: 'Please enter your phone number',
};

const _validationMap = {
    name: validateRequired,
    email: validateEmail,
    phone: validateRequired,
};

class FormCreateAccount extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                name: '',
                email: '',
                phone: '',
            }
        };
    }

    render() {
        let errorMessage = key => this.getValidationErrorMessage(this.state.validations, _errorMessages, key);
        return (
            <div className="form-create-account">
                <FormInput
                    label="Full Name"
                    id="name"
                    placeholder="e.g. Josie Smith"
                    value={this.state.values.name}
                    valid={this.state.validations.name}
                    onChange={this.handleChange.bind(this, 'name')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'name')}>
                    {errorMessage('name')}
                </FormInput>
                <FormInput
                    label="Email"
                    id="email"
                    type="email"
                    placeholder="e.g. josie.smith@mail.com"
                    value={this.state.values.email}
                    valid={this.state.validations.email}
                    onChange={this.handleChange.bind(this, 'email')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'email')}>
                    {errorMessage('email')}
                </FormInput>
                <FormInput
                    label="Mobile Phone"
                    id="phone"
                    type="tel"
                    placeholder="e.g. 07711223344"
                    value={this.state.values.phone}
                    valid={this.state.validations.phone}
                    onChange={this.handleChange.bind(this, 'phone')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'phone')}>
                    {errorMessage('phone')}
                </FormInput>
                <Button onClick={this.handleSubmit.bind(this, _validationMap)} className="btn-create-account btn-action-major">Create Account</Button>
            </div>
        );
    }
}

export default FormCreateAccount;
