import React from 'react';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormTagCloud from '../blocks/FormTagCloud';
import FormGeolocate from '../blocks/FormGeolocate';
import ValidatedComponent, { validateRequired, validateRequiredArray, validateEmail } from '../Validations';

const _errorMessages = {
    name: 'Please enter your full name',
    email: 'Please enter a valid email',
    phone: 'Please enter your phone number',
    location: 'Please enter your location, e.g. Edinburgh',
    headline: 'Please provide a quick introduction',
    cv: 'Please provide a short description of yourself',
    skills: 'Please list a couple of your key skills',
};

const _validationMap = {
    name: validateRequired,
    email: validateEmail,
    phone: validateRequired,
    location: validateRequired,
    headline: validateRequired,
    cv: validateRequired,
    skills: validateRequiredArray,
};

class FormWorkerProfile extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                name: '',
                email: '',
                phone: '',
                location: '',
                headline: '',
                cv: '',
                skills: [],
            },
            actionText: props.actionText ? props.actionText : 'Save'
        };
    }

    render() {
        let errorMessage = key => this.getValidationErrorMessage(this.state.validations, _errorMessages, key);
        return (
            <div className="form-worker-profile">
                <FormInput
                    label="Full Name"
                    id="name"
                    placeholder="e.g. Josie Smith"
                    value={this.state.values.name}
                    valid={this.state.validations.name}
                    onChange={this.handleChange.bind(this, 'name')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'name')}>
                    {errorMessage('name')}
                </FormInput>
                <FormInput
                    label="Email"
                    id="email"
                    type="email"
                    placeholder="e.g. josie.smith@mail.com"
                    value={this.state.values.email}
                    valid={this.state.validations.email}
                    onChange={this.handleChange.bind(this, 'email')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'email')}>
                    {errorMessage('email')}
                </FormInput>
                <FormInput
                    label="Mobile Phone"
                    id="phone"
                    type="tel"
                    placeholder="e.g. 07711223344"
                    value={this.state.values.phone}
                    valid={this.state.validations.phone}
                    onChange={this.handleChange.bind(this, 'phone')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'phone')}>
                    {errorMessage('phone')}
                </FormInput>
                <FormGeolocate
                    label="Location"
                    id="location"
                    placeholder="e.g. Edinburgh"
                    value={this.state.values.location}
                    valid={this.state.validations.location}
                    onChange={this.handleChange.bind(this, 'location')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'location')}>
                    {errorMessage('location')}
                </FormGeolocate>
                <FormInput
                    label="Headline"
                    id="headline"
                    placeholder="e.g. Waitress Extraordinaire!"
                    value={this.state.values.headline}
                    valid={this.state.validations.headline}
                    onChange={this.handleChange.bind(this, 'headline')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'headline')}>
                    {errorMessage('headline') || "Introduce yourself in one sentence"}
                </FormInput>
                <FormInput
                    label="Mini CV"
                    id="cv"
                    type="textarea"
                    placeholder="e.g. I've been a waitress for 5 years and pride myself on providing dazzling service"
                    value={this.state.values.cv}
                    valid={this.state.validations.cv}
                    onChange={this.handleChange.bind(this, 'cv')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'cv')}>
                    {errorMessage('cv') || "Tell us a little more about you"}
                </FormInput>
                <FormTagCloud
                    label="Skills"
                    id="skills"
                    placeholder="e.g. table waiting, confident"
                    tags={this.state.values.skills}
                    valid={this.state.validations.skills}
                    onChange={this.handleChange.bind(this, 'skills')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'skills')}>
                    {errorMessage('skills')}
                </FormTagCloud>
                <Button onClick={this.handleSubmit.bind(this, _validationMap)} className="btn-action-major">{this.state.actionText}</Button>
            </div>
        );
    }
}

export default FormWorkerProfile;
