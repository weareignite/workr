import React from 'react';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import ValidatedComponent, { validateRequired, validatePassword, validateMatch } from '../Validations';

const _errorMessages = {
    email: 'Please enter your email',
    password: 'Please enter a password',
    repeatPassword: 'Please repeat your password',
};

const _validationMap = {
    email: validateRequired,
    password: validatePassword,
    repeatPassword: [ validateMatch, 'password' ] // @NB: match field provided as second array param
};

class FormResetPassword extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                email: '',
                password: '',
                repeatPassword: '',
            }
        };
    }

    render() {
        let errorMessage = key => this.getValidationErrorMessage(this.state.validations, _errorMessages, key);
        return (
            <div className="form-forgot-password">
                <h1>Reset your password</h1>
                <FormInput
                    label="Email"
                    id="email"
                    type="email"
                    placeholder="e.g. josie.smith@mail.com"
                    value={this.state.values.email}
                    valid={this.state.validations.email}
                    onChange={this.handleChange.bind(this, 'email')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'email')}>
                    {errorMessage('email')}
                </FormInput>
                <FormInput
                    label="Password"
                    id="password"
                    type="password"
                    value={this.state.values.password}
                    valid={this.state.validations.password}
                    onChange={this.handleChange.bind(this, 'password')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'password')}>
                    {errorMessage('password') || 'Longer than six characters, with one number'}
                </FormInput>
                <FormInput
                    label="Repeat password"
                    id="repeatPassword"
                    type="password"
                    value={this.state.values.repeatPassword}
                    valid={this.state.validations.repeatPassword}
                    onChange={this.handleChange.bind(this, 'repeatPassword')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'repeatPassword')}>
                    {errorMessage('repeatPassword')}
                </FormInput>
                <Button onClick={this.handleSubmit.bind(this, _validationMap)} className="btn-reset-password btn-action-major">Reset Password</Button>
                <p className="text-center">
                    <a href="">back to login</a>
                </p>
            </div>
        );
    }
}

export default FormResetPassword;
