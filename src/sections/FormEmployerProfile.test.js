import React from 'react';
import { shallow, mount } from 'enzyme';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormSlider from '../blocks/FormSlider';
import FormDateRange from '../blocks/FormDateRange';
import FormTagCloud from '../blocks/FormTagCloud';
import FormEmployerProfile from './FormEmployerProfile';
import { validateRequired, validateRequiredArray, validateEmail } from '../Validations';
import moment from 'moment';

it('displays base element with correct class', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    expect( wrapper.find('div').at(0).hasClass('form-employer-profile') ).toBe(true);
});

it('displays business FormInput with correct label and id', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormInput).at(0).props();
    expect( props ).toHaveProperty('label', 'Business Name');
    expect( props ).toHaveProperty('id', 'business');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays job title FormInput with correct label and id', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormInput).at(1).props();
    expect( props ).toHaveProperty('label', 'Job Title');
    expect( props ).toHaveProperty('id', 'jobTitle');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays description FormInput with correct label and id', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormInput).at(2).props();
    expect( props ).toHaveProperty('label', 'Job Description');
    expect( props ).toHaveProperty('id', 'description');
    expect( props ).toHaveProperty('type', 'textarea');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays address FormInput with correct label and id', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormInput).at(3).props();
    expect( props ).toHaveProperty('label', 'Address');
    expect( props ).toHaveProperty('id', 'address');
    expect( props ).toHaveProperty('type', 'textarea');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays hourly rate FormSlider with correct label and format', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormSlider).at(0).props();
    expect( props ).toHaveProperty('label', 'Hourly Rate');
    expect( props ).toHaveProperty('valueFormat', '£%d');
    expect( props ).toHaveProperty('min', 5);
    expect( props ).toHaveProperty('max', 50);
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays FormDateRange', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormDateRange).at(0).props();
    expect( props ).toHaveProperty('startMinDate');
    expect( props ).toHaveProperty('finishMinDate');
    expect( props ).toHaveProperty('startDate');
    expect( props ).toHaveProperty('finishDate');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays skills FormTagCloud with correct label and id', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    const props = wrapper.find(FormTagCloud).at(0).props();
    expect( props ).toHaveProperty('label', 'Skills');
    expect( props ).toHaveProperty('id', 'skills');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('tags');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays find jobs Button with default text', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    expect( wrapper.find(Button).at(0).children().text() ).toBe('Save');
});

it('displays find jobs Button with correct class and text', () => {
    const wrapper = shallow(<FormEmployerProfile actionText="Find Jobs" />);
    const button = wrapper.find(Button).at(0);
    expect( button.props() ).toHaveProperty('className', 'btn-action-major');
    expect( button.children().text() ).toBe('Find Jobs');
});

let testSimulateInputChange = (key, index) => {
    const testValue = 'test me';
    const wrapper = mount(<FormEmployerProfile />);
    wrapper.find('Input').at(index).simulate('change', { target: { value: testValue }, persist: jest.fn() })
    expect( wrapper.state().values[key] ).toBe(testValue);
}

it('sets state when input name is updated', () => {
    testSimulateInputChange('jobTitle', 1);
});

it('sets state when input description is updated', () => {
    testSimulateInputChange('description', 2);
});

it('sets state when input hourly rate is updated', () => {
    const testValue = 21;
    const wrapper = shallow(<FormEmployerProfile />);
    wrapper.find('FormSlider').at(0).simulate('change', testValue);
    expect( wrapper.state().values.hourlyRate ).toBe(testValue);
});

it('sets state when input date range is updated', () => {
    const testValue = { start: moment(), finish: moment() };
    const wrapper = shallow(<FormEmployerProfile />);
    wrapper.find('FormDateRange').at(0).simulate('change', testValue);
    expect( wrapper.state().values.dateRange ).toBe(testValue);
});

it('sets state when input skills is updated', () => {
    const testValue = ['test', 'me'];
    const wrapper = shallow(<FormEmployerProfile />);
    wrapper.find('FormTagCloud').at(0).simulate('change', testValue);
    expect( wrapper.state().values.skills ).toBe(testValue);
});

it('triggers handleSubmit when Button is clicked if isValid returns true', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormEmployerProfile onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => true;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(1);
});

it('does not trigger handleSubmit when Button is clicked if isValid returns false', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormEmployerProfile onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => false;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(0);
});

it('isValid returns true if validations are true', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    expect( wrapper.instance().isValid({ name: value => true }) ).toBe(true);
});

it('isValid returns false if validations are false', () => {
    const wrapper = shallow(<FormEmployerProfile />);
    expect( wrapper.instance().isValid({ name: value => false }) ).toBe(false);
});
