import React from 'react';
import { Button } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import ValidatedComponent, { validateRequired, validatePassword } from '../Validations';

const _errorMessages = {
    email: 'Please enter your email',
    password: 'Please enter a password',
};

const _validationMap = {
    email: validateRequired,
    password: validatePassword,
};

class FormCreateAccount extends ValidatedComponent {
    constructor(props) {
        super(props);
        this.state = {
            values: {
                email: '',
                password: '',
            }
        };
    }

    render() {
        let errorMessage = key => this.getValidationErrorMessage(this.state.validations, _errorMessages, key);
        return (
            <div className="form-login">
                <h1>Sign into your account</h1>
                <FormInput
                    label="Email"
                    id="email"
                    type="email"
                    placeholder="e.g. josie.smith@mail.com"
                    value={this.state.values.email}
                    valid={this.state.validations.email}
                    onChange={this.handleChange.bind(this, 'email')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'email')}>
                    {errorMessage('email')}
                </FormInput>
                <FormInput
                    label="Password"
                    id="password"
                    type="password"
                    value={this.state.values.password}
                    valid={this.state.validations.password}
                    onChange={this.handleChange.bind(this, 'password')}
                    onValidate={this.handleValidate.bind(this, _validationMap, 'password')}>
                    {errorMessage('password') || 'Longer than six characters, with one number'}
                </FormInput>
                <Button onClick={this.handleSubmit.bind(this, _validationMap)} className="btn-login btn-action-major">Sign In</Button>
                <p className="text-center">
                    <a href="">forgot your password?</a>
                </p>
            </div>
        );
    }
}

export default FormCreateAccount;
