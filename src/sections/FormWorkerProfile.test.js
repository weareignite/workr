import React from 'react';
import { shallow, mount } from 'enzyme';
import { Button, Input, FormText } from 'reactstrap';
import FormInput from '../blocks/FormInput';
import FormTagCloud from '../blocks/FormTagCloud';
import FormGeolocate from '../blocks/FormGeolocate';
import FormWorkerProfile from './FormWorkerProfile';
import { validateRequired, validateRequiredArray, validateEmail } from '../Validations';

it('displays base element with correct class', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    expect( wrapper.find('div').at(0).hasClass('form-worker-profile') ).toBe(true);
});

it('displays name FormInput with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormInput).at(0).props();
    expect( props ).toHaveProperty('label', 'Full Name');
    expect( props ).toHaveProperty('id', 'name');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays email FormInput with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormInput).at(1).props();
    expect( props ).toHaveProperty('label', 'Email');
    expect( props ).toHaveProperty('id', 'email');
    expect( props ).toHaveProperty('type', 'email');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays phone FormInput with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormInput).at(2).props();
    expect( props ).toHaveProperty('label', 'Mobile Phone');
    expect( props ).toHaveProperty('id', 'phone');
    expect( props ).toHaveProperty('type', 'tel');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays location FormGeolocate with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormGeolocate).at(0).props();
    expect( props ).toHaveProperty('label', 'Location');
    expect( props ).toHaveProperty('id', 'location');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays headline FormInput with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormInput).at(3).props();
    expect( props ).toHaveProperty('label', 'Headline');
    expect( props ).toHaveProperty('id', 'headline');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays cv FormInput with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormInput).at(4).props();
    expect( props ).toHaveProperty('label', 'Mini CV');
    expect( props ).toHaveProperty('id', 'cv');
    expect( props ).toHaveProperty('type', 'textarea');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('value');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays skills FormTagCloud with correct label and id', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    const props = wrapper.find(FormTagCloud).at(0).props();
    expect( props ).toHaveProperty('label', 'Skills');
    expect( props ).toHaveProperty('id', 'skills');
    expect( props ).toHaveProperty('placeholder');
    expect( props ).toHaveProperty('tags');
    expect( props.onValidate.name ).toBe('bound handleValidate');
});

it('displays find jobs Button with default text', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    expect( wrapper.find(Button).at(0).children().text() ).toBe('Save');
});

it('displays find jobs Button with correct class and text', () => {
    const wrapper = shallow(<FormWorkerProfile actionText="Find Jobs" />);
    const button = wrapper.find(Button).at(0);
    expect( button.props() ).toHaveProperty('className', 'btn-action-major');
    expect( button.children().text() ).toBe('Find Jobs');
});

let testSimulateInputChange = (key, index) => {
    const testValue = 'test me';
    const wrapper = mount(<FormWorkerProfile />);
    wrapper.find('Input').at(index).simulate('change', { target: { value: testValue }, persist: jest.fn() })
    expect( wrapper.state().values[key] ).toBe(testValue);
}

it('sets state when input name is updated', () => {
    testSimulateInputChange('name', 0);
});

it('sets state when input email is updated', () => {
    testSimulateInputChange('email', 1);
});

it('sets state when input phone is updated', () => {
    testSimulateInputChange('phone', 2);
});

it('sets state when input headline is updated', () => {
    testSimulateInputChange('headline', 4);
});

it('sets state when input cv is updated', () => {
    testSimulateInputChange('cv', 5);
});

it('sets state when input location is updated', () => {
    const testValue = 'test me';
    const wrapper = shallow(<FormWorkerProfile />);
    wrapper.find('FormGeolocate').at(0).simulate('change', testValue);
    expect( wrapper.state().values.location ).toBe(testValue);
});

it('sets state when input skills is updated', () => {
    const testValue = ['test', 'me'];
    const wrapper = shallow(<FormWorkerProfile />);
    wrapper.find('FormTagCloud').at(0).simulate('change', testValue);
    expect( wrapper.state().values.skills ).toBe(testValue);
});

it('triggers handleSubmit when Button is clicked if isValid returns true', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormWorkerProfile onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => true;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(1);
});

it('does not trigger handleSubmit when Button is clicked if isValid returns false', () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(<FormWorkerProfile onSubmit={onSubmit} />);
    wrapper.instance().isValid = () => false;
    wrapper.find('Button').at(0).simulate('click');
    expect( onSubmit.mock.calls.length ).toBe(0);
});

it('isValid returns true if validations are true', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    expect( wrapper.instance().isValid({ name: value => true }) ).toBe(true);
});

it('isValid returns false if validations are false', () => {
    const wrapper = shallow(<FormWorkerProfile />);
    expect( wrapper.instance().isValid({ name: value => false }) ).toBe(false);
});
